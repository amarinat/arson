#version 330 core
out vec4 FragColor;

// Inputs
in vec2 textureCoordinates;
in vec3 normalCoordinates;
// Uniforms
uniform sampler2D sampleTexture0;	// albedo (procedural noise)
uniform sampler2D sampleTexture1;	// mask
uniform sampler2D sampleTexture2;	// alpha
uniform sampler2D sampleTexture3;	// distorsion
uniform float sTime;
uniform float cTime;
uniform vec3 angle;
uniform vec3 texOffset;
uniform vec3 texTile;
uniform float intensity;

// Aliases
vec4 m_albedo = texture(sampleTexture0, textureCoordinates);
vec4 m_mask = texture(sampleTexture1, textureCoordinates);
vec4 m_alpha = texture(sampleTexture2, textureCoordinates);
vec4 m_distorsionMap = texture(sampleTexture3, textureCoordinates);
vec2 m_tile = vec2(0.5,0.2);
vec2 m_offset = vec2(texOffset.x, texOffset.y);
vec2 m_uvOffset = vec2(m_distorsionMap.x, m_distorsionMap.y);
vec4 m_fresnel = vec4(0.9, 0.6, 0.3,1);
vec3 m_angle = angle;
vec2 m_unit = vec2(1,1);
vec2 m_identity = vec2(0,0);
float m_intensity = clamp(intensity, 0, 1);

// Headers
vec2 TileAndOffset(vec2 pos, vec2 tile, vec2 offset);
vec4 SampleAlbedo(vec2 texPosition);
vec4 ApplyMask(vec4 content, vec4 cutout);
vec4 ApplyFresnel(vec4 front, vec4 side, float power);

// Main logic

void main() {

///////////////////
// Debug values (not used yet)
m_offset = m_identity;
///////////////////////
	
	// UV mapping
	vec2 l_TiledOffsetted = TileAndOffset(textureCoordinates, m_tile, m_offset);
	vec2 l_correctedUV = m_uvOffset * 0.2;
	vec2 l_Distorted = TileAndOffset(l_TiledOffsetted, m_unit, l_correctedUV);
	
	// Texture modifications
	vec4 l_Texture = SampleAlbedo(l_Distorted);
	vec4 l_DepthAdd = ApplyFresnel(l_Texture, m_fresnel, 1.1);
	vec4 l_Masked = ApplyMask(l_Texture, m_mask);
	
	FragColor = l_Masked * m_intensity;
}

// Implementations

vec2 TileAndOffset(vec2 pos, vec2 tile, vec2 offset){
	return vec2(
		clamp(pos.x * tile.x + offset.x * sTime - 0.5, -1, 1),
		clamp(pos.y * tile.y + offset.y * cTime - 0.5, -1, 1));
}

vec4 SampleAlbedo(vec2 texPosition) {
	return texture(sampleTexture0, texPosition); 
}

vec4 ApplyMask(vec4 content, vec4 cutout) {
	return vec4(
		content.x,
		content.y,
		content.z,
		clamp(content.w, 0, cutout.x)); 
}

vec4 ApplyFresnel(vec4 front, vec4 side, float ratio){
	
	vec3 view = normalize(-m_angle);
	float refraction = dot(view, normalCoordinates);
	float power = pow(refraction, ratio);
	
	return mix(side, front, power);
}