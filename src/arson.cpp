#include "arson_internal.h"

#include <cstdlib>


namespace arson {

	struct Animation {
		mole::Vector3 position;
		float rotation;
		float speed;
		mole::Vector2 offset;
	};

	const mole::asset floor { "floor"};
	const mole::asset pit { "pit"};
	mole::asset fireShader;
	mole::asset mainCamera;
	mole::asset backupCamera;
	mole::Collection<Renderable> flames;
	mole::vector<Animation> animations;




	void Run() {

		mole::lib::Init();
		Load();
		mole::lib::SetUpdateCallback(Update);

		/* Camera callbacks */
		// horizontal movement: ASWD
		mole::lib::AttachCallbackToKey([] {MoveCam({ -1, 0, 0 }); }, 'A');
		mole::lib::AttachCallbackToKey([] {MoveCam({ 1, 0, 0 }); }, 'D');
		mole::lib::AttachCallbackToKey([] {MoveCam({ 0,0,1 }); }, 'W');
		mole::lib::AttachCallbackToKey([] {MoveCam({ 0,0,-1 }); }, 'S');
		// vertical movement: EQ
		mole::lib::AttachCallbackToKey([] {MoveCam({ 0,1,0 }); }, 'E');
		mole::lib::AttachCallbackToKey([] {MoveCam({ 0,-1,0 }); }, 'Q');
		// Rotation: mouse
		mole::lib::AttachCallbackToMousePosition([] {RotateCamera(); });
		// Field of view: mouse's back & forth
		mole::lib::AttachCallbackToMouseButton([] {UpdateCameraFieldOfView(true); }, 3);
		mole::lib::AttachCallbackToMouseButton([] {UpdateCameraFieldOfView(false); }, 4);
		// Camera reset: mouse main click
		mole::lib::AttachCallbackToMouseButton([] {ResetCamera(); }, 0);
		// Debug log: mouse secondary click
		mole::lib::AttachCallbackToMouseButton([] {PrintCamera(); }, 1);

		mole::lib::Run();

		mole::lib::Unload();
		mole::lib::Finalize();
	}

	void Animate(int index);

	void Load() {

		// Shaders
		asset defaultShader{ mole::lib::Load(Resource::Shader,"shaders/default") };
		fireShader = { mole::lib::Load(Resource::Shader,"shaders/fire") };

		// Procedural textures
		asset noiseGenerator{ mole::lib::Load(Resource::Procedural,"shaders/noise") };

		// Textures
		asset distorsion{ mole::lib::Load(Resource::Texture,"resources/distorsion.png") };
		asset alpha{ mole::lib::Load(Resource::Texture,"resources/mask.png") }; // unused
		asset mask{ mole::lib::Load(Resource::Texture,"resources/mask.png") };
		asset pattern{ mole::lib::Load(Resource::Texture,"resources/concrete.png") };
		asset burnt{ mole::lib::Load(Resource::Texture,"resources/burnt.png") };
		vector<asset> fireTextures {
			noiseGenerator,		// tex0
			mask,				// tex1
			alpha,				// tex2
			distorsion, 		// tex3
		};

		vector<asset> floorTextures { pattern};
		vector<asset> pitTextures { burnt};

		// Cameras
		mainCamera = { mole::lib::Load(Resource::Camera,"resources/test.cam") };
		backupCamera = { mole::lib::Load(Resource::Camera,"resources/test.cam") };

		// Meshes
		asset quadMesh{ mole::lib::Load(Resource::Mesh,"resources/quad.obj") };
		asset pitMesh{ mole::lib::Load(Resource::Mesh,"resources/pit.obj") };
		asset flameMesh = { mole::lib::Load(Resource::Mesh,"resources/flame.obj") };

		// Transforms
		asset floorTransform{ mole::lib::Load(Resource::Transform, nullptr) };
		asset pitTransform{ mole::lib::Load(Resource::Transform, nullptr) };

		for (int i = 0; i < 50; ++i) {
			asset flameName{ "flame_" + std::to_string(i) };
			asset fireTransform{ mole::lib::Load(Resource::Transform,nullptr) };
			mole::Callback update{ [=]() { Animate(i); } };
			flames.push_back(mole::lib::GenerateRenderable(flameName, RenderableType::Model, fireShader, flameMesh, fireTextures, mainCamera, fireTransform, update));
		}

		Collection<Renderable> newRenderables{flames};
		newRenderables.push_back(mole::lib::GenerateRenderable(floor, RenderableType::Sprite, defaultShader, quadMesh, floorTextures, mainCamera, floorTransform, [] {}));
		newRenderables.push_back(mole::lib::GenerateRenderable(pit, RenderableType::Model, defaultShader, pitMesh, pitTextures, mainCamera, pitTransform, [] {}));
		Collection<Generator> newGenerators{
			mole::lib::GetGenerator(noiseGenerator),
		};

		mole::lib::SetActiveRenderables(newRenderables, newGenerators);

		for (Renderable* flame : flames) {
			Animation animation{};
			animation.position = {
				1 - (std::rand() % 200) / 100.0f,
				(std::rand() % 600) / 100.0f,
				1 - (std::rand() % 300) / 100.0f };
			animation.rotation = { (float)(std::rand() % 360) };
			animation.offset = {
				0,
				0 };
			animation.speed = { 2 + 3 * (std::rand() % 100) / 100.0f };
			animations.push_back(animation);

			Transform& t{ flame->transform };
			const mole::Vector3& p{ animation.position};
			t.position = { p.x, p.y, p.z };
			t.eulerRotation.y = { animation.rotation };
		}
		{
			Renderable* f{ mole::lib::GetRenderable(floor) };
			Transform& transform{ f->transform };

			const float s{ 25.5 };
			transform.scale = { s,s,s };
		}
		{
			Renderable* f{ mole::lib::GetRenderable(pit) };
			Transform& transform{ f->transform };

			const float s{ 2 };
			transform.scale = { s,s,s };
		}
	}

	void Update() {
		mole::Camera* cam { mole::lib::GetCamera(mainCamera)};
		mole::Shader* fire {mole::lib::GetShader(fireShader)};
		const mole::Vector3 viewingAngle{ cam->GetDirection()};
		fire->use();
		fire->setVec3("angle", viewingAngle);
	}

	void Animate(int i) {
		Renderable* flame{ flames[i] };
		Transform& t{ flame->transform };
		mole::Vector3& p {t.position};
		Animation& a{ animations[i] };

		float y = { (float)(p.y + a.speed * mole::lib::GetDeltaTime()) };
		if (y > 3 * a.speed) {
			y = 0;
			a.position = {
				1 - (std::rand() % 200) / 100.0f,
				y,
				1 - (std::rand() % 300) / 100.0f };
			a.speed = { 2 + 3 * (std::rand() % 100) / 100.0f };
		}

		t.position = {p.x,y,p.z};
		const float intensity{ 3 * a.speed - y };
		flame->shader->use();
		flame->shader->setFloat("intensity", intensity);
	}

	void MoveCam(const mole::Vector3 direction) {

		Camera& c{ *mole::lib::GetCamera(mainCamera) };
		const mole::Vector3 increment{ direction.x / 5, direction.y / 5, direction.z / 5 };

		c.Translate(increment);
	}

	void RotateCamera() {

		Camera& c{ *mole::lib::GetCamera(mainCamera) };

		const mole::Vector2 m{ mole::lib::GetMouseDeltaPosition()};
		const mole::Vector3 n{ m.x / 10.0f, -m.y / 10.0f, 0};

		c.Rotate(n);
	}

	void UpdateCameraFieldOfView(bool increment) {

		Camera& c{ *mole::lib::GetCamera(mainCamera) };

		const int speed{ 1 };
		const float f{ c.fov() + speed * increment - speed * !increment };

		c.SetFieldOfView(f);
	}

	void ResetCamera() {
		Camera& o{ *mole::lib::GetCamera(backupCamera) };
		Camera& c{ *mole::lib::GetCamera(mainCamera) };

		c.SetTransform(o.transform().position, o.transform().eulerRotation, o.transform().scale);
	}

	void PrintCamera() {

		Camera& c{ *mole::lib::GetCamera(mainCamera) };
		mole::lib::ConsoleLog("\n### Camera settings:"
			"\n\tposition:\t[" + std::to_string(c.transform().position.x) + "," + std::to_string(c.transform().position.y) + "," + std::to_string(c.transform().position.z) + string("] ")
			+ "\n\trotation:\t[" + std::to_string(c.transform().eulerRotation.x) + "," + std::to_string(c.transform().eulerRotation.y) + "," + std::to_string(c.transform().eulerRotation.z) + string("] ")
			+ "\n\tfov:\t\t[" + std::to_string(c.fov()) + "]"
			+ "\n\tmode:\t\t[" + (c.mode() == mole::CameraMode::Perspective ? "PERSPECTIVE" : "UNKNOWN") + string("]\n###")
		);
	}
}