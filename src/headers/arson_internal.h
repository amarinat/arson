#ifndef ARSON_INTERNAL
#define ARSON_INTERNAL

#include "arson.h"

#include "moleLib.h";

namespace arson {

	using asset = mole::asset;
	using string = mole::string;
	using Camera = mole::Camera;
	using Transform = mole::Transform;
	using Renderable = mole::Renderable;
	using Generator = mole::Generator;
	using RenderableType = mole::RenderableType;
	using Resource = mole::Resource;
	template<class T> using vector = mole::vector<T>;
	template<class T> using Collection = mole::Collection<T>;

	void Load();
	void Update();

	void MoveCam(const mole::Vector3 direction);
	void RotateCamera();
	void UpdateCameraFieldOfView(bool increment);
	void ResetCamera();
	void PrintCamera();
}

#endif //!ARSON_INTERNAL