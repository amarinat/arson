#include "moleSTB_internal.h"

namespace mole {

	void STB::LogError(string message) {
		Log::Error("MoleSTB", message);
	}

	void STB::LogInfo(string message) {
		Log::Info("MoleSTB", message);
	}

	void STB::LogDebug(string message) {
		Log::Debug("MoleSTB", message);
	}
}
