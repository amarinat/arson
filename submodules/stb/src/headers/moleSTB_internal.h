#ifndef MOLESTB_INTERNAL
#define MOLESTB_INTERNAL

#include "moleSTB.h"

namespace mole::STB
{
	void LogError(string message);
	void LogInfo(string message);
	void LogDebug(string message);
};

#endif //!MOLESTB_INTERNAL