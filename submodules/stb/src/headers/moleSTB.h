#ifndef MOLESTB
#define MOLESTB

#include "moleCommons.h"

namespace mole::STB {

	class TextureLoader;
}

class mole::STB::TextureLoader {

	static unsigned int idCount;
	const unsigned int id;
	RawTexture bytes;
	int width;
	int height;

public:
	TextureLoader(const char* path, bool flip);
	~TextureLoader();

	const RawTexture Data() const;
	unsigned int getWidth() const;
	unsigned int getHeight() const;
	string ToString() const;
};

#endif //!MOLESTB