#include "moleSTB_internal.h"

extern "C" {
#define STB_IMAGE_IMPLEMENTATION
#include "stb_image.h"
}

namespace mole {

	unsigned int STB::TextureLoader::idCount{ 0 };

	STB::TextureLoader::TextureLoader(const char* path, bool flip = true) :
		id{ ++idCount } {

		int numCol;

		stbi_set_flip_vertically_on_load(flip);
		bytes = static_cast<RawTexture>(stbi_load(path, &width, &height, &numCol, 0));

		if (bytes) {
			LogDebug(string("Image successfully loaded from ") + path + "(" + this->ToString() + ")");
		}
		else {
			LogError(string("Failed to load texture at ") + path);
		}
	}

	STB::TextureLoader::~TextureLoader() {
		stbi_image_free(bytes);
		LogDebug(this->ToString() + string(" freed"));
	}

	const RawTexture STB::TextureLoader::Data() const {
		return bytes;
	}

	string STB::TextureLoader::ToString() const {
		return string("MoleSTD Texture#") + std::to_string(id);
	}

	unsigned int STB::TextureLoader::getWidth() const {
		return width;
	}

	unsigned int STB::TextureLoader::getHeight() const {
		return height;
	}

}