#include "molelib_internal.h"

namespace mole {

	void lib::Init() {
		width = 640;
		height = 480;
		windowName = "ARSON - Amarinat";

		InitContext();
		InitRender();
		context.AttachCallbackToKey([] { running = false; }, 0);
		SetUpdateCallback([] {});

		failShader = { Load(Resource::Shader,"shaders/fail") };
		// TO DO other assets
		if (!shaderRefs[failShader]->Loaded()) {
			running = true;
			LogError("Back-end unitialized, but failed to load failback assets.");
		}
		else {
			running = true;
			LogInfo("Back-end initialized");
		}
	}

	void lib::Finalize() {
		FinalizeRender();
		FinalizeContext();
		LogInfo("Back-end finalized");
	}

	asset lib::Load(Resource type, const char* path) {
		asset ref;

		switch (type)
		{
		case Resource::Shader: {

			owned<Shader> shader {LoadShader(path)};
			const asset tempRef{ shader->ToString() };
			Shader* rawPointer{ shader.get() };

			if (!rawPointer->Loaded()) {
				LogError("Failed to load shader at path: " + string(path));
				ref = { failShader };
			}
			else {
				shaders.push_back(std::move(shader));
				ref = { tempRef };
			}

			shaderRefs.insert({ ref, rawPointer });

			break;
		}
		case Resource::Procedural: {

			owned<Generator> generator{ LoadGenerator(path) };

			Generator* rawPointer{ generator.get() };
			Shader* rawShaderPointer{ static_cast<Shader*>(generator.get()) };
			Texture* rawTexturePointer{ static_cast<Texture*>(generator.get()) };
			const string tempRef{ rawPointer->ToString() };
			ref = { tempRef }; // shader and texture refs are discarded

			generators.push_back(std::move(generator));
			shaderRefs.insert({ ref, rawShaderPointer });
			textureRefs.insert({ ref, rawTexturePointer });
			generatorRefs.insert({ ref, rawPointer });

			if (!shaderRefs[ref]->Loaded()) {
				LogError("Failed to load procedural generator at path: " + string(path));
			}
			break;
		}
		case Resource::Texture: {

			owned<Texture> texture{ LoadTexture(path) };

			Texture* rawPointer{ texture.get() };
			const string tempRef{ rawPointer->ToString() };
			ref = { tempRef };
			textures.push_back(std::move(texture));
			textureRefs.insert({ ref, rawPointer });

			break;
		}
		case Resource::Mesh: {

			owned<Mesh> mesh{ LoadMesh(path)};
			Mesh* rawPointer{ mesh.get() };
			const string tempRef{ rawPointer->ToString() };
			ref = { tempRef };
			meshes.push_back(std::move(mesh));
			meshRefs.insert({ ref, rawPointer });

			break;
		}
		case Resource::Camera: {

			owned<Camera> camera{ LoadCamera(path)};
			Camera* rawPointer{ camera.get() };
			const string tempRef{ rawPointer->ToString() };
			ref = { tempRef };
			cameras.push_back(std::move(camera));
			cameraRefs.insert({ ref, rawPointer });

			break;
		}
		case Resource::Transform: {

			owned<Transform> transform{ CreateTransform()};
			Transform* rawPointer{ transform.get() };
			const string tempRef{ rawPointer->ToString() };
			ref = { tempRef };
			transforms.push_back(std::move(transform));
			transformRefs.insert({ ref, rawPointer });

			break;
		}
		default:
			LogError(string("Unknown resource type at ") + string(path));
			ref = { "invalid" };
			break;
		}

		return ref;
	}

	void lib::Unload() {

		for (owned<Shader>& shader : shaders) {
			shader->Unload();
		}

		for (owned<Generator>& generator : generators) {
			static_cast<Shader*>(generator.get())->Unload();
		}

		for (auto& model : meshes) {
			model->Unload();
		}

		for (auto& texture : textures) {
			texture->Unload();
		}

		LogInfo("Unloaded all the resources");
	}

	void lib::Run() {
		LogInfo(string("Starting to render with [")
			+ std::to_string(activeRenderables.size()) + "] renderable and ["
			+ std::to_string(activeGenerators.size()) + "] procedurally generated elements");

		while (running) {

			context.PollEvents();

			const float time{ (float)context.GetTime() };
			for (auto& shader : shaders) {
				shader->use();
				shader->setFloat("lTime", time);
				shader->setFloat("sTime", sin(time));
				shader->setFloat("cTime", cos(time));
			}
			for (auto& generator : generators) {
				generator->use();
				generator->setFloat("lTime", time);
				generator->setFloat("sTime", sin(time));
				generator->setFloat("cTime", cos(time));
			}

			update();
			render.Render();
			context.Display();
			context.Sync();
		}
	}

	void lib::SetUpdateCallback(Callback callback) {
		update = callback;
	}

	owned<Shader> lib::LoadShader(const char* path) {

		owned<Shader> shader{ std::make_unique<GLAD::GladShader>((string(path) + ".vshad").c_str(), (string(path) + ".fshad").c_str()) };

		LogInfo(string("Shader loaded from: ") + string(path));
		return std::move(shader);
	}

	owned<Generator> lib::LoadGenerator(const char* path) {

		owned<Generator> generator{ std::make_unique<GLAD::GladGenerator>((string(path) + ".vshad").c_str(), (string(path) + ".fshad").c_str()) };

		LogInfo(string("Generator loaded from: ") + string(path));
		return std::move(generator);
	}

	owned<Camera> lib::LoadCamera(const char* path) {

		owned<Camera> camera{ std::make_unique<GLAD::GladCamera>(path) };

		LogInfo(string("Camera loaded from: ") + string(path));
		return std::move(camera);
	}

	owned<Texture> lib::LoadTexture(const char* path) {

		STB::TextureLoader loader{ mole::STB::TextureLoader(path, true) };
		owned<Texture> texture{ std::make_unique<GLAD::GladTexture>(loader.Data(), loader.getWidth(), loader.getHeight(), TextureInterpolation::Closest) };

		LogInfo(string("Texture loaded from: ") + string(path));
		return texture;
	}

	owned<Mesh> lib::LoadMesh(const char* path) {
		owned<Mesh> mesh{ std::make_unique<GLAD::GladMesh>(path) };

		LogInfo(string("Mesh loaded from: ") + string(path));
		return mesh;
	}

	owned<Transform> lib::CreateTransform() {
		owned<Transform> transform{ std::make_unique<Transform>() };

		LogInfo(string("Transform created with id ") + std::to_string(transform->id));
		return transform;
	}

	Renderable* lib::GenerateRenderable(asset name, RenderableType type, asset shader, asset mesh, vector<asset> textures, asset camera, asset transform, Callback callback) {

		Shader* s{ GetShader(shader) };
		Mesh* m{ GetMesh(mesh) };
		Textures usedTextures{};
		for (asset& texture : textures) {
			usedTextures.push_back(textureRefs[texture]);
		}
		Camera* c{ GetCamera(camera) };
		Transform* t{ GetTransform(transform) };

		owned<Renderable> renderable {std::make_unique<Renderable>(name, type, s, m, usedTextures, c, *t, callback)};
		Renderable* rawPointer{ renderable.get() };
		renderables.push_back(std::move(renderable));
		renderableRefs.insert({ rawPointer->name, rawPointer });

		return rawPointer;
	}

	void lib::SetActiveRenderables(Collection<Renderable> newBatch, Collection<Generator> newGenerators) {
		activeRenderables.clear();
		activeGenerators.clear();

		for (Generator* g : newGenerators) {
			activeGenerators.push_back(g);
		};

		for (Renderable* r : newBatch) {
			activeRenderables.push_back(r);
		}

		mole::GLAD::SetActiveElements(activeRenderables, activeGenerators);
	}

	void lib::AttachCallbackToKey(Callback fun, char key) {
		context.AttachCallbackToKey(fun, key);
	}

	void lib::AttachCallbackToMousePosition(Callback fun) {
		context.AttachCallbackToMousePosition(fun);
	}

	void lib::AttachCallbackToMouseButton(Callback fun, int button) {
		context.AttachCallbackToMouseButton(fun, button);
	}

	double lib::GetTime() {
		return context.GetTime();
	}

	double lib::GetDeltaTime() {
		return context.GetDeltaTime();
	}

	Shader* lib::GetShader(asset name) {
		return shaderRefs[name];
	}

	Texture* lib::GetTexture(asset name) {
		return textureRefs[name];
	}

	Generator* lib::GetGenerator(asset name) {
		return generatorRefs[name];
	}

	Camera* lib::GetCamera(asset name) {
		return cameraRefs[name];
	}

	Mesh* lib::GetMesh(asset name) {
		return meshRefs[name];
	}

	Transform* lib::GetTransform(asset name) {
		return transformRefs[name];
	}

	Renderable* lib::GetRenderable(asset name) {
		return renderableRefs[name];
	}

	const Vector2& lib::GetMouseDeltaPosition() {
		return context.GetMousePosition();
	}

	void lib::InitContext() {
		context.CreateContext(width, height, windowName);
	}

	void lib::FinalizeContext() {
		context.FinalizeContext();
	}

	void lib::InitRender() {
		render.Load(width, height);
	}

	void lib::FinalizeRender() {
		render.Unload();
	}

	void lib::LogError(string message) {
		Log::Error("MoleLIB", message);
	}

	void lib::LogInfo(string message) {
		Log::Info("MoleLIB", message);
	}

	void lib::ConsoleLog(string message) {
		Log::Bypass(message);
	}
}