#ifndef MOLELIB
#define MOLELIB

#include "moleCommons.h"

namespace mole::lib {

	void Init();
	void Finalize();
	void Run();

	[[nodiscard]] asset Load(Resource type, const char* path);
	void Unload();

	Renderable* GenerateRenderable(asset name, RenderableType type, asset shader, asset mesh, vector<asset> textures, asset camera, asset transform, Callback callback);

	void SetUpdateCallback(Callback callback);
	void SetActiveRenderables(Collection<Renderable> newBatch, Collection<Generator> generators);

	void AttachCallbackToKey(Callback fun, char key);
	void AttachCallbackToMousePosition(Callback fun);
	void AttachCallbackToMouseButton(Callback fun, int button);

	Shader* GetShader(asset name);
	Texture* GetTexture(asset name);
	Generator* GetGenerator(asset name);
	Camera* GetCamera(asset name);
	Mesh* GetMesh(asset name);
	Transform* GetTransform(asset name);


	[[nodiscard]] double GetTime();
	[[nodiscard]] double GetDeltaTime();
	const Vector2& GetMouseDeltaPosition();
	Renderable* GetRenderable(asset name);
	void ConsoleLog(string message);

	/*
	template<class Transform>
	component<Transform> Load(string path) {
		Load(Resource::Transform, path.c_str());

		return component<Transform>{};
	}
	template<class R>
	component<R> Load(string path) {
		mole::Log::Error("MoleLib Templates", "Requested type lacks specialization");
		return component<R>();
	}
	*/
}

#endif //!MOLELIB