#ifndef MOLELIB_INTERNAL
#define MOLELIB_INTERNAL

#include "molelib.h"

#include "moleGlad.h"
#include "MoleGLFW.h"
#include "MoleSTB.h"

namespace mole::lib {

	// TO DO dependency injection
	using Context = GLFW::GLFWContext;
	using Render = GLAD::GladRenderer;;
	using Textures = Collection<Texture>;
	using Renderables = Collection<Renderable>;

	Context context;
	Render render;
	Repository<Shader> shaders;
	Repository<Generator> generators;
	Repository<Texture> textures;
	Repository<Mesh> meshes;
	Repository<Transform> transforms;
	Repository<Camera> cameras;
	Repository<Renderable> renderables;

	Locator<asset, Shader> shaderRefs;
	Locator<asset, Texture> textureRefs;
	Locator<asset, Generator> generatorRefs;
	Locator<asset, Mesh> meshRefs;
	Locator<asset, Transform> transformRefs;
	Locator<asset, Camera> cameraRefs;
	Locator<asset, Renderable> renderableRefs;

	Renderables activeRenderables;
	Collection<Generator> activeGenerators;
	Callback update;

	unsigned width;
	unsigned height;
	const char* windowName;
	bool running;

	asset failShader;

	void InitContext();
	void FinalizeContext();
	void InitRender();
	void FinalizeRender();

	[[nodiscard]] owned<Shader> LoadShader(const char* path);
	[[nodiscard]] owned<Generator> LoadGenerator(const char* path);
	[[nodiscard]] owned<Texture> LoadTexture(const char* path);
	[[nodiscard]] owned<Camera> LoadCamera(const char* path);
	[[nodiscard]] owned<Mesh> LoadMesh(const char* path);
	[[nodiscard]] owned<Transform> CreateTransform();

	void LogError(string message);
	void LogInfo(string message);
}

#endif //!MOLELIB_INTERNAL