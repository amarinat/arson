#ifndef MOLEGLFW_INTERNAL
#define MOLEGLFW_INTERNAL

#include "MoleGLFW.h"

extern "C" {

#include "glfw3.h"
}


namespace mole::GLFW {

	static void ErrorCallback(int code, const char* message);
	static void KeyCallback(GLFWwindow* window, int key, int scancode, int action, int mods);
	static void MouseButtonCallback(GLFWwindow* window, int button, int action, int mods);
	static void MouseCursorPosCallback(GLFWwindow* window, double x, double y);

	GLFWwindow* window;
	/*
	GLFWmonitor* monitor;
	*/
	Vector2 delta;

	void LogError(string message);
	void LogInfo(string message);
	void LogDebug(string message);
}

#endif //!MOLEGLFW_INTERNAL