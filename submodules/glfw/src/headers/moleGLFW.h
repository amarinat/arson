#ifndef MOLEGLFW
#define MOLEGLFW

#include "moleCommons.h"
#include <string>

namespace mole::GLFW {

	struct GLFWContext;
}

struct mole::GLFW::GLFWContext
{
	bool KeyPressed(char key);
	void AttachCallbackToKey(Callback fun, char key);
	void AttachCallbackToMousePosition(Callback fun);
	void AttachCallbackToMouseButton(Callback fun, int button);

	void CreateContext(unsigned width, unsigned height, string name);
	void FinalizeContext();

	void PollEvents();
	void Display();
	void Sync();

	double GetTime();
	double GetDeltaTime();
	const Vector2& GetMousePosition() const;
private:
	double previousTime;
	double deltaTime;
};

#endif //!MOLEGLFW