#include "moleGLFW_internal.h"

#include "moleCommons.h"
#include <functional>

namespace mole {

	static GLFW::GLFWContext context{};
	map<char, vector<Callback>> callbacks;
	vector<Callback> mouseScrollCallbacks;
	map<int, vector<Callback>> mouseButtonCallbacks;

	bool GLFW::GLFWContext::KeyPressed(char key) {
		LogError("Key pressing check not implemented, returning false");
		return false;
	}

	double GLFW::GLFWContext::GetTime() {
		return glfwGetTime();
	}
	
	double GLFW::GLFWContext::GetDeltaTime() {
		return deltaTime;
	}

	const Vector2& GLFW::GLFWContext::GetMousePosition() const {
		return delta;
	}

	void GLFW::GLFWContext::PollEvents() {
		glfwPollEvents();
	}

	void GLFW::GLFWContext::Display() {
		glfwSwapBuffers(window);
	}

	void GLFW::GLFWContext::Sync() {

		double targetTime{ previousTime + 1.0 / 24.0 };
		while (targetTime > GetTime()) {
			// wait (busy loop)
		}
		deltaTime = GetTime() - previousTime;
		previousTime = GetTime();
	}

	void GLFW::GLFWContext::AttachCallbackToKey(Callback fun, char key) {

		if (!callbacks.count(key)) {
			callbacks.insert({ key, vector<Callback> {} });
		}

		vector<Callback>& actions = callbacks.at(key);
		actions.push_back(fun);
	}

	void GLFW::GLFWContext::AttachCallbackToMousePosition(Callback fun) {
		mouseScrollCallbacks.push_back(fun);
	}

	void GLFW::GLFWContext::AttachCallbackToMouseButton(Callback fun, int button) {

		if (!mouseButtonCallbacks.count(button)) {
			mouseButtonCallbacks.insert({ button, vector<Callback> {} });
		}

		vector<Callback>& actions = mouseButtonCallbacks.at(button);
		actions.push_back(fun);
	}

	void GLFW::GLFWContext::CreateContext(unsigned width, unsigned height, string name)
	{
		glfwSetErrorCallback(ErrorCallback);

		glfwInit();
		glfwWindowHint(GLFW_SAMPLES, 4);
		glfwWindowHint(GLFW_DEPTH_BITS, 16);
		glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 4);
		glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 6);
		glfwWindowHint(GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE);
		glfwWindowHint(GLFW_OPENGL_DEBUG_CONTEXT, GL_TRUE);

		window = glfwCreateWindow(width, height, name.c_str(), NULL, NULL);
		if (!window) {
			glfwTerminate();
			LogError("Failed to create the window");
		}
		else {
			/* Full Screen mode
			monitor = glfwGetPrimaryMonitor();
			const GLFWvidmode* mode = glfwGetVideoMode(monitor);
			glfwSetWindowMonitor(window, monitor, 0, 0, mode->width, mode->height, mode->refreshRate);
			*/

			glfwSetWindowPos(window, 200, 10);

			glfwSetKeyCallback(window, KeyCallback);
			glfwSetMouseButtonCallback(window, MouseButtonCallback);
			glfwSetCursorPosCallback(window, MouseCursorPosCallback);

			glfwSetInputMode(window, GLFW_CURSOR, GLFW_CURSOR_DISABLED);

			glfwMakeContextCurrent(window);
			glfwSwapInterval(1);

			previousTime = glfwGetTime();

			LogDebug("context initialized");
		}
	}

	void GLFW::GLFWContext::FinalizeContext() {
		glfwDestroyWindow(window);
		glfwTerminate();

		LogDebug("context terminated");
	}

	void GLFW::ErrorCallback(int code, const char* message) {
		Log::Error("GLFW", std::to_string(code) + ' ' + string(message));
	}

	void GLFW::KeyCallback(GLFWwindow* window, int key, int scancode, int action, int mods) {
		LogDebug("key pressed: [" + std::to_string((char)key) + "]");

		if (callbacks.count(key)) {
			vector<Callback>& actions = callbacks.at(key);
			for (Callback& action : actions) {
				action();
			}
		}
	}

	void GLFW::MouseButtonCallback(GLFWwindow* window, int button, int action, int mods) {
		LogDebug("Mouse button pressed: [" + std::to_string(button) + "]");

		if (mouseButtonCallbacks.count(button)) {
			auto& actions = mouseButtonCallbacks.at(button);
			for (Callback& action : actions) {
				action();
			}
		}
	}

	void GLFW::MouseCursorPosCallback(GLFWwindow* window, double x, double y) {
		LogDebug("MouseCursorPosCallback: [" + std::to_string(x) + "][" + std::to_string(y) + "]");

		static Vector2 previous {(float)x, (float)y};

		const Vector2 c{ (float)x, (float)y };
		delta = { c.x - previous.x, c.y - previous.y };
		previous = c;

		for (Callback& callback : mouseScrollCallbacks) {
			callback();
		}
	}

	void GLFW::LogError(string message) {
		Log::Error("MoleGLFW", message);
	}

	void GLFW::LogInfo(string message) {
		Log::Info("MoleGLFW", message);
	}

	void GLFW::LogDebug(string message) {
		Log::Debug("MoleGLFW", message);
	}
}