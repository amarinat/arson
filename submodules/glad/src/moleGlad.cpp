#include "moleGlad_internal.h"

#include "GladObjects.h"
#include <cstdarg>

namespace mole::GLAD {

	bool loaded{ false };
	vector<owned<GladDrawCall>> drawCalls;
	vector<owned<GladBufferRenderer>> proceduralRenderers;

	void GladRenderer::Load(unsigned width, unsigned height) {

		const bool glLoaded{ 0 != gladLoadGL() };

		if (!glLoaded) {

			LogError("OpenGL renderer failed to load");
		}
		else {

			glDepthFunc(GL_LEQUAL);
			glEnable(GL_DEPTH_TEST);

			glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
			glEnable(GL_BLEND);
			/*
			glCullFace(GL_BACK);
			glFrontFace(GL_CCW);
			glEnable(GL_CULL_FACE);
			*/

			auto callback = [](const char* name, void* fun, int argc, ...) {
				GLenum code{ glad_glGetError() };
				va_list args;
				va_start(args, argc);
				GLenum arg0 = va_arg(args, GLenum);
				va_end(args);

				if (code != GL_NO_ERROR) {
					LogGladError((string("\u001b[1;36mOpenGL error code [") + std::to_string(arg0) + "] in " + name + std::string("\u001b[1;0m")).c_str());
				}
				else {
					LogGladDebug((string("\u001b[1;36mOpenGL callback: [") + std::to_string(arg0) + "] in " + name + std::string("\u001b[1;0m")).c_str());

				}
			};
			glad_set_post_callback(callback);

			loaded = true;
			LogDebug("OpenGL context successfully loaded");
		}
	}

	void GladRenderer::Unload() {

		if (loaded) {
			drawCalls.clear();
			proceduralRenderers.clear();
			
			LogDebug("OpenGL context successfully unloaded");
		}
		else {
			LogWarning("Trying to unload OpenGL non-loaded OpenGL context");
		}
	}

	void GladRenderer::Render() {

		LogDebug("Starting frame render");

		for (auto& r : proceduralRenderers) {
			r->Draw();
		}

		glClearColor(0, 0, 0, 1);
		glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
		for (owned<GladDrawCall>& r : drawCalls) {
			r->Draw();
		}
	}

	void SetActiveElements(Collection<Renderable> renderables, Collection<Generator> proceduralRendereres) {
		GenerateBufferRenderers(proceduralRendereres);
		GenerateDrawCalls(renderables);
	}

	void UnloadRenderables() {

		for (auto& r : proceduralRenderers) {
			r->Unload();
		}

		for (auto& drawcall : drawCalls) {
			drawcall->Unload();
		}
	}


	void GenerateDrawCalls(Collection<Renderable> renderables) {
		for (Renderable* r : renderables) {
			drawCalls.push_back(std::make_unique<GladDrawCall>(r->name, r->type, *r->shader, *r->mesh, r->textures, *r->camera, &r->transform, r->update));
		}
	}

	void GenerateBufferRenderers(Collection<Generator> generators) {
		for (Generator* g : generators) {
			proceduralRenderers.push_back(std::make_unique<GladBufferRenderer>(g, 64));
		}
	}

	void LogGladError(string message) {
		Log::Error("Glad", message);
	}

	void LogGladDebug(string message) {
		Log::Debug("Glad", message);
	}

	void LogError(string message) {
		Log::Error("MoleGlad", message);
	}

	void LogWarning(string message) {
		Log::Warning("MoleGlad", message);
	}

	void LogInfo(string message) {
		Log::Info("MoleGlad", message);
	}

	void LogDebug(string message) {
		Log::Debug("MoleGlad", message);
	}
}