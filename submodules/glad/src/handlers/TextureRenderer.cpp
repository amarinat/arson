#include "moleglad_internal.h"

#include <fstream>
#include <sstream>

namespace mole::GLAD {

	GladTextureRenderer::GladTextureRenderer(const char* vertexPath, const char* fragmentPath) :
		loaded{ false } {

		string vertexCode;
		string fragmentCode;
		std::ifstream vShaderFile;
		std::ifstream fShaderFile;

		vShaderFile.exceptions(std::ifstream::failbit | std::ifstream::badbit);
		fShaderFile.exceptions(std::ifstream::failbit | std::ifstream::badbit);
		try
		{
			LogDebug(string("opening vertex shader at path: ") + vertexPath);
			vShaderFile.open(vertexPath);
			LogDebug(string("opening fragment shader at path: ") + fragmentPath);
			fShaderFile.open(fragmentPath);

			std::stringstream vShaderStream, fShaderStream;
			vShaderStream << vShaderFile.rdbuf();
			fShaderStream << fShaderFile.rdbuf();
			vShaderFile.close();
			fShaderFile.close();

			vertexCode = vShaderStream.str();
			fragmentCode = fShaderStream.str();

			LogDebug("Shader files successfully loaded");
		}
		catch (std::ifstream::failure e)
		{
			LogError("Some shaders failed to load. Check the following files:");
			LogError(vertexPath);
			LogError(fragmentPath);

			if (vShaderFile.is_open()) vShaderFile.close();
			if (fShaderFile.is_open()) fShaderFile.close();
		}

		const GLchar* vShaderCode{ vertexCode.c_str() };
		const GLchar* fShaderCode{ fragmentCode.c_str() };

		int success;
		char infoLog[512];

		unsigned int vertex{ glCreateShader(GL_VERTEX_SHADER) };
		glShaderSource(vertex, 1, &vShaderCode, NULL);
		glCompileShader(vertex);
		glGetShaderiv(vertex, GL_COMPILE_STATUS, &success);
		if (success) {
			LogDebug("Vertex shader successfully compiled");
		}
		else {
			glGetShaderInfoLog(vertex, 512, NULL, infoLog);
			LogError("Vertex shader failed to compile:");
			LogGladError(infoLog);
		}

		unsigned int fragment{ glCreateShader(GL_FRAGMENT_SHADER) };
		glShaderSource(fragment, 1, &fShaderCode, NULL);
		glCompileShader(fragment);
		glGetShaderiv(fragment, GL_COMPILE_STATUS, &success);
		if (success) {
			LogDebug("Fragment shader successfully compiled");
		}
		else {
			glGetShaderInfoLog(fragment, 512, NULL, infoLog);
			LogError("Fragment shader failed to compile");
			LogGladError(infoLog);
		}

		id = glCreateProgram();
		glAttachShader(id, vertex);
		glAttachShader(id, fragment);
		glLinkProgram(id);

		glGetProgramiv(id, GL_LINK_STATUS, &success);
		if (success) {
			loaded = true;
			LogDebug(string("Shader successfully linked and attached with id ") + std::to_string(id));
		}
		else {
			glGetShaderInfoLog(vertex, 512, NULL, infoLog);
			LogError("Shader program failed to link");
			LogGladError(infoLog);
		}

		glDeleteShader(vertex);
		glDeleteShader(fragment);
	}

	GladTextureRenderer::~GladTextureRenderer() {
		if (loaded) {
			LogWarning("texture renderer " + std::to_string(id) + " was destroyed while still loaded");
			unload();
		}
	}

	void GladTextureRenderer::use() {
		glUseProgram(id);
		LogDebug("using texture renderer with id " + std::to_string(id));
	}

	void GladTextureRenderer::unload() {
		glDeleteProgram(id);
		loaded = false;
		LogDebug("unloaded texture renderer with id " + std::to_string(id));
	}

	void GladTextureRenderer::setBool(const string& name, bool value) const {
		glUniform1i(glGetUniformLocation(id, name.c_str()), (int)value);
	}

	void GladTextureRenderer::setInt(const string& name, int value) const {
		glUniform1i(glGetUniformLocation(id, name.c_str()), value);
	}

	void GladTextureRenderer::setFloat(const string& name, float value) const {
		glUniform1f(glGetUniformLocation(id, name.c_str()), value);
	}

	unsigned int GladTextureRenderer::getId() const {
		return id;
	}

	const bool GladTextureRenderer::Loaded() const {
		return loaded;
	}
}