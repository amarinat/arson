#include "moleglad_internal.h"

#include <fstream>
#include <sstream>

namespace mole::GLAD {

	unsigned int GladGenerator::nextRef{0};

	GladGenerator::GladGenerator(const char* vertexPath, const char* fragmentPath) :
		textureId{ 0 },
		ref{ ++nextRef },
		textureType{ GL_TEXTURE_2D },
		loaded{ false } {

		string vertexCode;
		string fragmentCode;
		std::ifstream vShaderFile;
		std::ifstream fShaderFile;

		vShaderFile.exceptions(std::ifstream::failbit | std::ifstream::badbit);
		fShaderFile.exceptions(std::ifstream::failbit | std::ifstream::badbit);
		try {
			LogDebug(string("opening vertex shader at path: ") + vertexPath);
			vShaderFile.open(vertexPath);
			LogDebug(string("opening fragment shader at path: ") + fragmentPath);
			fShaderFile.open(fragmentPath);

			std::stringstream vShaderStream, fShaderStream;
			vShaderStream << vShaderFile.rdbuf();
			fShaderStream << fShaderFile.rdbuf();
			vShaderFile.close();
			fShaderFile.close();

			vertexCode = vShaderStream.str();
			fragmentCode = fShaderStream.str();

			LogDebug("Shader files successfully loaded");
		}
		catch (std::ifstream::failure e) {
			LogError(
				string("Some generators failed to load. Check the following files:")
				+ "\n\t" + string(vertexPath)
				+ "\n\t" + string(fragmentPath));

			if (vShaderFile.is_open()) vShaderFile.close();
			if (fShaderFile.is_open()) fShaderFile.close();
		}

		const GLchar* vShaderCode{ vertexCode.c_str() };
		const GLchar* fShaderCode{ fragmentCode.c_str() };

		int success;
		char infoLog[512];

		unsigned int vertex{ glCreateShader(GL_VERTEX_SHADER) };
		glShaderSource(vertex, 1, &vShaderCode, NULL);
		glCompileShader(vertex);
		glGetShaderiv(vertex, GL_COMPILE_STATUS, &success);
		if (success) {
			LogDebug("Vertex shader successfully compiled");
		}
		else {
			glGetShaderInfoLog(vertex, 512, NULL, infoLog);
			LogError("Vertex shader failed to compile:");
			LogGladError(infoLog);
		}

		unsigned int fragment{ glCreateShader(GL_FRAGMENT_SHADER) };
		glShaderSource(fragment, 1, &fShaderCode, NULL);
		glCompileShader(fragment);
		glGetShaderiv(fragment, GL_COMPILE_STATUS, &success);
		if (success) {
			LogDebug("Fragment shader successfully compiled");
		}
		else {
			glGetShaderInfoLog(fragment, 512, NULL, infoLog);
			LogError("Fragment shader failed to compile");
			LogGladError(infoLog);
		}

		shaderId = glCreateProgram();
		glAttachShader(shaderId, vertex);
		glAttachShader(shaderId, fragment);
		glLinkProgram(shaderId);

		glGetProgramiv(shaderId, GL_LINK_STATUS, &success);
		if (success) {
			loaded = true;
			LogDebug(string("Shader successfully linked and attached with id ") + std::to_string(shaderId));
		}
		else {
			glGetShaderInfoLog(vertex, 512, NULL, infoLog);
			LogError("Shader program failed to link");
			LogGladError(infoLog);
		}

		glDeleteShader(vertex);
		glDeleteShader(fragment);
	}

	GladGenerator::~GladGenerator() {
		if (loaded) {
			LogWarning("texture renderer " + std::to_string(shaderId) + " was destroyed while still loaded");
			Unload();
		}
	}

	void GladGenerator::use() {
		glUseProgram(shaderId);
		LogDebug("using texture renderer with id " + std::to_string(shaderId));
	}

	void GladGenerator::Unload() {
		glDeleteProgram(shaderId);
		loaded = false;
		LogDebug("unloaded texture renderer with id " + std::to_string(shaderId));
	}

	void GladGenerator::setBool(const string& name, bool value) const {
		glUniform1i(glGetUniformLocation(shaderId, name.c_str()), (int)value);
	}

	void GladGenerator::setInt(const string& name, int value) const {
		glUniform1i(glGetUniformLocation(shaderId, name.c_str()), value);
	}

	void GladGenerator::setFloat(const string& name, float value) const {
		glUniform1f(glGetUniformLocation(shaderId, name.c_str()), value);
	}

	void GladGenerator::setVec3(const string& name, Vector3 value) const {
		glUniform3f(glGetUniformLocation(shaderId, name.c_str()),value.x, value.y, value.z);
	}

	void GladGenerator::setMat4(const string& name, float* value) const {
		const GLint loc{ glGetUniformLocation(shaderId, name.c_str()) };
		glUniformMatrix4fv(loc, 1, GL_FALSE, value);
	}

	const unsigned int GladGenerator::getShaderId() const {
		return shaderId;
	}

	const unsigned int GladGenerator::getTextureId() const {
		return textureId;
	}

	void GladGenerator::SetTextureId(unsigned int id) {
		textureId = id;
	}

	const bool GladGenerator::Loaded() const {
		return loaded;
	}

	void GladGenerator::Bind() {
		LogDebug("Binding " + ToString() + " (as a texture)");
		glBindTexture(textureType, textureId);
	}

	void GladGenerator::Unbind() {
		LogDebug("Binding " + ToString() + " (as a texture)");
		glBindTexture(textureType, 0);
	}

	void GladGenerator::BindToUnit(mole::Shader& shader, const char* uniform, unsigned int unit) {
		LogDebug("Binding " + shader.ToString() + " to [" + ToString() + "] in unit [" + std::to_string(unit) + "] with uniform string [" + string(uniform) + "]");

		GLuint unitRef{ GL_TEXTURE0 + unit };
		glActiveTexture(unitRef);
		Bind();
		shader.setInt(uniform, unit);
	}

}