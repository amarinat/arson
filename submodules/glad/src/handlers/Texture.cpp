#include "moleGlad_internal.h"

namespace mole::GLAD {

	unsigned int GladTexture::nextRef{0};

	GladTexture::GladTexture(const RawTexture data, unsigned int width, unsigned int height, TextureInterpolation interpolation = TextureInterpolation::Linear) :
		type{ GL_TEXTURE_2D },
		ref{ ++nextRef } {
		LogDebug("Generating " + ToString());
		// Texture generation
		glGenTextures(1, &id);
		Bind();

		// Filter setting
		int mode;
		switch (interpolation) {
		case TextureInterpolation::Closest:
			mode = GL_LINEAR;
			break;
		default:
			mode = GL_NEAREST;
			break;
		}
		glTexParameteri(type, GL_TEXTURE_MIN_FILTER, mode);
		glTexParameteri(type, GL_TEXTURE_MAG_FILTER, mode);

		// Border clamp
		glTexParameteri(type, GL_TEXTURE_WRAP_S, GL_REPEAT);
		glTexParameteri(type, GL_TEXTURE_WRAP_T, GL_REPEAT);

		// Texture upload and GPU calculations
		GLenum format{ GL_RGB };
		GLenum pixelType{ GL_UNSIGNED_BYTE };
		glTexImage2D(type, 0, GL_RGB, width, height, 0, format, pixelType, data);
		glGenerateMipmap(type);

		// TO DO handle loading errors
		loaded = true;
		LogDebug(string("Unless a GLAD error occurred, successfuly loaded ")
			+ ToString()
			+ string(" with size [")
			+ std::to_string(width)
			+ string("]x[")
			+ std::to_string(height)
			+ string("]"));

		Unbind();
	}

	GladTexture::~GladTexture() {
		if (loaded) {
			LogWarning("Trying to destroy a texture while still loaded, in texture id " + std::to_string(id));
			Unload();

		}
	}

	void GladTexture::Unload() {

		if (loaded) {
			glDeleteTextures(1, &id);
			loaded = false;
			LogDebug("Unloaded texture id " + std::to_string(id));
		}
		else {
			LogWarning("Trying to unload already unloaded texture id " + std::to_string(id));
		}

	}

	void GladTexture::BindToUnit(mole::Shader& shader, const char* uniform, unsigned int unit) {
		LogDebug("Binding texture for " + shader.ToString() + " in unit [" + std::to_string(unit) + "] with uniform string [" + string(uniform) + "]");

		GLuint unitRef{ GL_TEXTURE0 + unit };
		glActiveTexture(unitRef);
		Bind();
		shader.setInt(uniform, unit);
	}

	void GladTexture::Bind() {
		LogDebug(string("binding ") + ToString());
		glBindTexture(type, id);
	}

	void GladTexture::Unbind() {
		LogDebug(string("unbinding ") + ToString());
		glBindTexture(type, 0);
	}

	const unsigned int GladTexture::getTextureId() const {
		return id;
	}
}