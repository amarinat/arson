#include "moleglad_internal.h"

#include "moleMath.h"
#include <fstream>
#include <sstream>

#include <iostream>
namespace mole::GLAD {

	unsigned int GladCamera::nextId{ 0 };

	GladCamera::GladCamera(const char* path) :
		id{ ++nextId },
		Camera{} {

		Vector3 position{ 0,0,0 };
		Vector3 rotation{ 0,0,0 };
		Vector3 scale{ 1,1,1 };

		std::ifstream content;
		content.exceptions(std::ifstream::failbit | std::ifstream::badbit);

		try {
			LogDebug(string("opening camera file at path: ") + path);
			content.open(path);

			string toParse;
			float x, y, z;

			content >> x;
			content >> y;
			content >> z;
			position = { x, y, z };
			LogDebug(string("Parsed camera position: ")
				+ "[" + std::to_string(position.x) + "]"
				+ "[" + std::to_string(position.y) + "]"
				+ "[" + std::to_string(position.z) + "]");

			content >> x;
			content >> y;
			content >> z;
			rotation = { x, y, z };
			LogDebug(string("Parsed camera rotation: ")
				+ "[" + std::to_string(front().x) + "]"
				+ "[" + std::to_string(front().y) + "]"
				+ "[" + std::to_string(front().z) + "]");

			content >> m_fov;
			LogDebug("Parsed camera field of view: " + std::to_string(fov()));

			int modeValue;
			content >> modeValue;
			m_mode = static_cast<CameraMode>(modeValue);
			LogDebug("Parsed camera mode: " + std::to_string(static_cast<int>(m_mode)));

			while (content.good()) {
				string next;
				content >> next;
				LogWarning(string("Unparsed information left in ifstream:") + next);
			}

			content.close();

			LogDebug("Successfuly loaded camera with id " + std::to_string(id));
		}
		catch (std::ifstream::failure e) {

			LogError(string("Failed to load camera at path:") + path + string(", default settings will be applied"));

			if (content.is_open()) {
				content.close();
			}

			m_mode = CameraMode::Perspective;
		}

		SetTransform(position, rotation, scale);
		SetFieldOfView(m_fov);
	}

	GladCamera::~GladCamera() {
	}

	void GladCamera::SetTransform(const Vector3& position, const Vector3& rotation, const Vector3& scale) {
		m_transform.position = position;
		m_transform.eulerRotation = rotation;
		m_transform.scale = scale;

		view = Math::GetViewMatrix(*this);
	}

	void GladCamera::Translate(const Vector3 d) {

		const Transform& t{ transform() };
		const mole::Vector3 p{ t.position};

		const mole::Vector3 cup{ up() };
		const mole::Vector3 cright{ right() };
		const mole::Vector3 cfront{ front()};

		mole::Vector3 f;
		f.x = p.x - d.x * cright.x + d.z * cfront.x;
		f.y = p.y + d.y;
		f.z = p.z + d.x * cright.z + d.z * cfront.z;

		SetTransform(f, t.eulerRotation, t.scale);
	}

	void GladCamera::Rotate(const Vector3 increment) {

		const Transform& t{ m_transform };
		const Vector3& c{ t.eulerRotation };

		const Vector3 clamped{
			Math::mod(c.x + increment.x, 360.0f, -360.f),
			Math::clamp(c.y - increment.y, 70.0f, -70.0f),
			Math::mod(c.z + increment.z,  360.0f, -360.0f),
		};

		SetTransform(t.position, clamped, t.scale);
	}

	void GladCamera::SetFieldOfView(float fov) {
		m_fov = Math::clamp(fov, 160, 1);

		projection = Math::GeProjectionMatrix(m_mode, m_fov, 16 / 9, 0.1, 100); // TO DO link with settings
	}

	void GladCamera::SetMode(CameraMode mode) {
		m_mode = mode;

		projection = Math::GeProjectionMatrix(m_mode, m_fov, 16 / 9, 0.1, 100); // TO DO link with settings
	}


	float* GladCamera::GetView() {
		return view;
	}

	float* GladCamera::GetProjection() {
		return projection;
	}

	const Vector3 GladCamera::GetDirection() const {
		return front();
	}

	const Transform& GladCamera::transform() const {
		return m_transform;
	}

	const Vector3 GladCamera::front() const {
		return Math::GetFront(*this);
	}

	const Vector3 GladCamera::up() const {
		return Math::GetUp(*this);
	}

	const Vector3 GladCamera::right() const {
		return Math::GetRight(*this);
	}

	const float GladCamera::fov() const {
		return m_fov;
	}

	const CameraMode GladCamera::mode() const {
		return m_mode;
	}

	unsigned int GladCamera::getId() const {
		return id;
	}
}