#include "moleGlad_internal.h"

#include "moleCommons.h"
#include "GladObjects.h"
#include <fstream>
#include <sstream>

namespace mole::GLAD {

	unsigned int GladMesh::nextId{ 0 };

	GladMesh::GladMesh(const char* path) :
		smoothness{},
		id{ ++nextId } {

		//Mesh parse verbosity is set to silent by default since it clutters the console
		Verbosity previous{ Log::GetVerbosity() };
		Log::SetVerbosity(Verbosity::Silent);

		std::ifstream content;
		try {

			Parse(path, content);
			loaded = true;
			Log::SetVerbosity(previous);
			LogDebug("Successfuly parsed Mesh  " + ToString());

		}
		catch (std::ifstream::failure e) {

			Log::SetVerbosity(previous);
			LogError(string("Failed to parse Mesh at path:") + path);
			loaded = false;

			if (content.is_open()) {
				content.close();
			}
		}
	}

	GladMesh::~GladMesh() {
		if (loaded) {
			LogWarning("mesh " + std::to_string(id) + " was destroyed while still loaded");
			Unload();
		}
	}

	void GladMesh::Unload() {
		// TODO
		loaded = false;
	}

	void GladMesh::Parse(const char* path, std::ifstream& content) {

		LogDebug(string("opening Mesh file at path: ") + path);
		content.open(path);


		string toParse;
		float x, y, z;
		int objectCount{ 0 };
		int smoothnessCount{ 0 };

		char type{};

		while (content.good()) {

			content >> type;
			switch (type)
			{
			case '#':

				if (Log::GetVerbosity() < Verbosity::Debug) {

					// discard
					while (content.good() && content.peek() != '\n') {
						content.ignore();
					}
					content.ignore();
				}
				else {
					vector<string> comment;
					string t{ type };
					string s{ ' ' };

					comment.push_back(t);

					while (content.peek() != '\n') {

						content >> toParse;
						comment.push_back(s);
						comment.push_back(toParse);
					}

					toParse.clear();

					for (string chunk : comment) {
						toParse += chunk;
					}

					LogDebug("Ignoring comment: " + toParse);
				}
				break;
			case 'o':
				++objectCount;
				if (objectCount > 1) {

					LogError(string("loading multiple object in a single .obj file is still not supported, at ") + path);
					throw;
				}
				else {
					// discard
					LogDebug("Ignoring object name (o)");
					while (content.peek() != '\n') {
						content.ignore();
					}
					content.ignore();
				}
				break;
			case 's':
				++smoothnessCount;
				if (smoothnessCount > 1) {

					LogWarning(string("loading multiple smoothness values in a single .obj file is not supported, at ") + path);
				}
				else {

					content >> smoothness;
					LogDebug("Parsing smoothness: " + std::to_string(smoothness));

					while (content.peek() != '\n') {
						content.ignore();
					}
					content.ignore();
				}
				break;
			case 'f':

				unsigned int v, n, t;
				while (content.good() && content.peek() != '\n') {
					content >> v;
					content.get();
					content >> t;
					content.get();
					content >> n;
					LogDebug(string("pushing vertex: " + std::to_string(v) + "/" + std::to_string(t) + "/" + std::to_string(n)));
					protoVertices.push_back({ v,n,t });

				}
				break;
			case 'v': {

				char subtype{ 'v' };
				switch (content.peek()) {
				case 't': subtype = 't'; content.get(); break;
				case 'n': subtype = 'n'; content.get(); break;
				default: break;
				}

				int i{ 0 };
				float next[4]{ };
				while (content.good() && content.peek() != '\n') {

					content >> next[i];
					++i;
				}
				LogDebug("parsed vertex v(" + string{ subtype } + "): [" + std::to_string(next[0]) + "][" + std::to_string(next[1]) + "][" + std::to_string(next[2]) + "]");

				switch (subtype) {
				case 'v': vertices.push_back({ next[0], next[1], next[2] });; break;
				case 't': textures.push_back({ next[0], next[1] }); break;
				case 'n': normals.push_back({ next[0], next[1], next[2] }); break;
				default: LogError("trying to parse an invalid vertex subtype: " + string{ subtype }); break;
				}

				break;
			}

			default:
				string ts{ type };
				LogWarning("unexpected type: " + ts);
				if (Log::GetVerbosity() < Verbosity::Debug) {
					// discard
					while (content.good() && content.peek() != '\n') {
						content.ignore();
					}
					content.ignore();
				}
				else {
					vector<string> discard;
					string s{ ' ' };
					while (content.good() && content.peek() != '\n') {

						content >> toParse;
						discard.push_back(s);
						discard.push_back(toParse);
					}
					toParse.clear();
					for (string chunk : discard) {
						toParse += chunk;
					}

					LogDebug("Ignoring content: " + toParse);
				}
				break;
			}
		}

		content.close();
	}

	const vector<float> GladMesh::GenerateData() const {
		vector<float> buffer{};

		try {

			// TODO custom vertex structures
			for (const ProtoVertex& proto : protoVertices) {

				//vertex
				const Vector3& v{ vertices[proto.vertex - 1] };
				buffer.push_back(v.x);
				buffer.push_back(v.y);
				buffer.push_back(v.z);

				//normal
				const Vector3& n{ normals[proto.normal - 1] };
				buffer.push_back(n.x);
				buffer.push_back(n.y);
				buffer.push_back(n.z);

				//uv
				const Vector2& t{ textures[proto.texture - 1] };
				buffer.push_back(t.x);
				buffer.push_back(t.y);
			}
		}
		catch (...) {
			LogError("Unable to generate data, error at index: " + std::to_string(buffer.size()));
		}

		return buffer;
	}

	const vector<unsigned int> GladMesh::GenerateIndices() const {

		vector<unsigned int> buffer{};

		for (int i = 0; i < protoVertices.size(); ++i) {

			buffer.push_back((i + 0));

			/* TO DO non-triangulated quads
			buffer.push_back((i + 1) % 4);
			buffer.push_back((i + 2) % 4);
			buffer.push_back((i + 2) % 4);
			buffer.push_back((i + 3) % 4);
			buffer.push_back((i + 4) % 4);
			*/
		}

		return buffer;
	}

	const unsigned int GladMesh::GetDataSize() const {
		return protoVertices.size();
	}
}