#include "moleglad_internal.h"

#include <fstream>
#include <sstream>

namespace mole::GLAD {

	unsigned int GladShader::nextRef{0};

	GladShader::GladShader(const char* vertexPath, const char* fragmentPath) :
		ref{ ++nextRef },
		loaded{ false } {

		string vertexCode;
		string fragmentCode;
		std::ifstream vShaderFile;
		std::ifstream fShaderFile;

		vShaderFile.exceptions(std::ifstream::failbit | std::ifstream::badbit);
		fShaderFile.exceptions(std::ifstream::failbit | std::ifstream::badbit);
		try {
			LogDebug(string("opening vertex shader at path: ") + vertexPath);
			vShaderFile.open(vertexPath);
			LogDebug(string("opening fragment shader at path: ") + fragmentPath);
			fShaderFile.open(fragmentPath);

			std::stringstream vShaderStream, fShaderStream;
			vShaderStream << vShaderFile.rdbuf();
			fShaderStream << fShaderFile.rdbuf();
			vShaderFile.close();
			fShaderFile.close();

			vertexCode = vShaderStream.str();
			fragmentCode = fShaderStream.str();

			LogDebug("Shader files successfully loaded");
		}
		catch (std::ifstream::failure e) {
			LogError(
				string("Some shaders failed to load. Check the following files:")
				+ "\n\t" + string(vertexPath)
				+ "\n\t" + string(fragmentPath));

			if (vShaderFile.is_open()) vShaderFile.close();
			if (fShaderFile.is_open()) fShaderFile.close();
		}

		const GLchar* vShaderCode{ vertexCode.c_str() };
		const GLchar* fShaderCode{ fragmentCode.c_str() };

		int success;
		char infoLog[512];

		unsigned int vertex{ glCreateShader(GL_VERTEX_SHADER) };
		glShaderSource(vertex, 1, &vShaderCode, NULL);
		glCompileShader(vertex);
		glGetShaderiv(vertex, GL_COMPILE_STATUS, &success);
		if (success) {
			LogDebug("Vertex shader successfully compiled");
		}
		else {
			glGetShaderInfoLog(vertex, 512, NULL, infoLog);
			LogError("Vertex shader failed to compile:");
			LogGladError(infoLog);
		}

		unsigned int fragment{ glCreateShader(GL_FRAGMENT_SHADER) };
		glShaderSource(fragment, 1, &fShaderCode, NULL);
		glCompileShader(fragment);
		glGetShaderiv(fragment, GL_COMPILE_STATUS, &success);
		if (success) {
			LogDebug("Fragment shader successfully compiled");
		}
		else {
			glGetShaderInfoLog(fragment, 512, NULL, infoLog);
			LogError("Fragment shader failed to compile");
			LogGladError(infoLog);
		}

		id = glCreateProgram();
		glAttachShader(id, vertex);
		glAttachShader(id, fragment);
		glLinkProgram(id);

		glGetProgramiv(id, GL_LINK_STATUS, &success);
		if (success) {
			loaded = true;
			LogDebug(string("Shader successfully linked and attached with id ") + std::to_string(id));
		}
		else {
			glGetShaderInfoLog(vertex, 512, NULL, infoLog);
			LogError("Shader program failed to link");
			LogGladError(infoLog);
		}

		glDeleteShader(vertex);
		glDeleteShader(fragment);
	}

	GladShader::~GladShader() {
		if (loaded) {
			LogWarning(ToString() + " was destroyed while still loaded");
			Unload();
		}
	}

	void GladShader::use() {
		LogDebug("using  " + ToString());
		glUseProgram(id);
	}

	void GladShader::Unload() {
		glDeleteProgram(id);
		loaded = false;
		LogDebug("unloaded " + ToString());
	}

	void GladShader::setBool(const string& name, bool value) const {
		LogDebug("setting bool with value [" + string(value ? "true" : "false") + string("] at ") + ToString());
		const GLint loc{ glGetUniformLocation(id, name.c_str()) };
		glUniform1i(loc, (int)value);
	}

	void GladShader::setInt(const string& name, int value) const {
		LogDebug("setting int in [" + name + "] with value [" + std::to_string(value) + string("] at ") + ToString());
		const GLint loc{ glGetUniformLocation(id, name.c_str()) };
		glUniform1i(loc, value);
	}

	void GladShader::setFloat(const string& name, float value) const {
		LogDebug("setting float with value [" + std::to_string(value) + string("] at ") + ToString());
		const GLint loc{ glGetUniformLocation(id, name.c_str()) };
		glUniform1f(loc, value);
	}

	void GladShader::setVec3(const string& name, Vector3 value) const {
		LogDebug(string("setting Vector3 with value ")
			+ "[" + std::to_string(value.x)
			+ "|" + std::to_string(value.y)
			+ "|" + std::to_string(value.z)
			+ string("] at ") + ToString());
		const GLint loc{ glGetUniformLocation(id, name.c_str()) };
		glUniform3f(loc, value.x, value.y, value.z);
	}

	void GladShader::setMat4(const string& name, float* value) const {
		float* i{ value };
		LogDebug("setting 4x4 matrix at " + ToString() + " with values:\n"
			+ "\t" + std::to_string(*(i++)) + " " + std::to_string(*(i++)) + " " + std::to_string(*(i++)) + " " + std::to_string(*(i++)) + "\n"
			+ "\t" + std::to_string(*(i++)) + " " + std::to_string(*(i++)) + " " + std::to_string(*(i++)) + " " + std::to_string(*(i++)) + "\n"
			+ "\t" + std::to_string(*(i++)) + " " + std::to_string(*(i++)) + " " + std::to_string(*(i++)) + " " + std::to_string(*(i++)) + "\n"
			+ "\t" + std::to_string(*(i++)) + " " + std::to_string(*(i++)) + " " + std::to_string(*(i++)) + " " + std::to_string(*(i++)));

		const GLint loc{ glGetUniformLocation(id, name.c_str()) };
		glUniformMatrix4fv(loc, 1, GL_FALSE, value);
	}

	const unsigned int GladShader::getShaderId() const {
		return id;
	}

	const bool GladShader::Loaded() const {
		return loaded;
	}
}