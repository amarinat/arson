#include "GladObjects.h"

namespace mole::GLAD {

	RBO::RBO() :
		GladBuffer("RBO") {

		glGenRenderbuffers(1, &id);
	}

	RBO::~RBO() {
		glDeleteBuffers(1, &id);
		glDeleteRenderbuffers(1, &id);
	}

	void RBO::Attach(unsigned int size) {
		LogDebug("Attaching " + ToString()); 
		glRenderbufferStorage(GL_RENDERBUFFER, GL_DEPTH24_STENCIL8, size, size);
		glFramebufferRenderbuffer(GL_FRAMEBUFFER, GL_DEPTH_STENCIL_ATTACHMENT, GL_RENDERBUFFER, id);
	}

	void RBO::Bind() {
		LogDebug("Binding " + ToString());
		glBindRenderbuffer(GL_RENDERBUFFER, id);
	}

	void RBO::Unbind() {
		LogDebug("Unbinding " + ToString());
		glBindRenderbuffer(GL_RENDERBUFFER, 0);
	}

	void RBO::UpdateBuffer(const vector<GLuint>& indices, unsigned int mode) {
		const GLsizeiptr size = indices.size() * sizeof(GLuint);
		const GLuint* data = indices.data();

		glBufferData(GL_ELEMENT_ARRAY_BUFFER, size, data, mode);
	}
}