#include "GladObjects.h"

namespace mole::GLAD {

	VBO::VBO() :
		GladBuffer("VBO") {

		glGenBuffers(1, &id);
	}

	VBO::~VBO() {
		vector<GLuint> buffers{ this->id };
		glDeleteBuffers(buffers.size(), buffers.data());
	}

	void VBO::Bind() {
		LogDebug("Binding " + ToString());
		glBindBuffer(GL_ARRAY_BUFFER, id);
	}

	void VBO::Unbind() {
		LogDebug("Unbinding " + ToString());
		glBindBuffer(GL_ARRAY_BUFFER, 0);
	}

	void VBO::UpdateBuffer(const vector<GLfloat>& vertices, int drawType) {
		const GLsizeiptr size{ static_cast<GLsizeiptr>(vertices.size() * sizeof(GLfloat)) };
		const GLfloat* data{ vertices.data() };

		glBufferData(GL_ARRAY_BUFFER, size, data, drawType);
	}
}