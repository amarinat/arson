#include "GladObjects.h"

namespace mole::GLAD {

	unsigned int GladBuffer::nextRef {0};

	GladBuffer::GladBuffer(string name) :
		ref{ ++nextRef },
		name{ name } {

		LogDebug("Loading " + ToString());
	}

	GladBuffer::~GladBuffer() {
		LogDebug("destroyed " + ToString());
	}


	const GLuint GladBuffer::GetId() const {
		return id;
	}

	const string GladBuffer::ToString() const {
		return "\u001b[1;35m" + name + "#" + std::to_string(ref) + "\u001b[1;0m";
	}
}