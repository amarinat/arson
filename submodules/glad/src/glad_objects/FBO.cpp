#include "GladObjects.h"

namespace mole::GLAD {

	FBO::FBO(unsigned int size) :
		width{ size },
		height{ size },
		GladBuffer{ "FBO" } {

		glGenFramebuffers(1, &id);
	}

	FBO::~FBO() {
		glDeleteFramebuffers(1, &id);
	}

	void FBO::Bind() {
		LogDebug("Binding " + ToString());
		glBindFramebuffer(GL_FRAMEBUFFER, id);
	}

	void FBO::Unbind() {
		LogDebug("Unbinding " + ToString());
		glBindFramebuffer(GL_FRAMEBUFFER, 0);
	}
}