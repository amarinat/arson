#include "GladObjects.h"

namespace mole::GLAD {

	EBO::EBO() :
		indexCount{},
		GladBuffer{ "EBO" } {

		glGenBuffers(1, &id);
	}

	EBO::~EBO() {
		vector<GLuint> buffers{ this->id };
		glDeleteBuffers(buffers.size(), buffers.data());
	}

	void EBO::Bind() {
		LogDebug("Binding " + ToString());
		glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, id);
	}

	void EBO::Unbind() {
		LogDebug("Unbinding " + ToString());
		glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, 0);
	}

	const unsigned int EBO::size() const {
		return indexCount;
	}

	void EBO::UpdateBuffer(const vector<GLuint>& indices) {
		const GLsizeiptr size{ static_cast<GLsizeiptr>(indices.size() * sizeof(GLuint)) };
		const GLuint* data{ indices.data() };

		glBufferData(GL_ELEMENT_ARRAY_BUFFER, size, data, GL_STATIC_DRAW);
		indexCount = indices.size();
	}
}