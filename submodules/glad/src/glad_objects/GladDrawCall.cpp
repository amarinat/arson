#include "GladObjects.h"
#include "moleMath.h"

namespace mole::GLAD {

	unsigned int GladDrawCall::nextId{ 0 };

	GladDrawCall::GladDrawCall(asset emmiter, RenderableType type, Shader& shader, Mesh& mesh, Textures textures, Camera& camera, Transform* transform, Callback update) :
		emmiter{ emmiter },
		vao{},
		vbo{},
		ebo{},
		shader{ shader },
		mesh{ mesh },
		textures{ textures },
		camera{ camera },
		transform{ transform },
		update{ update },
		id{ ++nextId } {

		/* Geometry binding */
		vao.Bind(); // TO DO batch rendering

		vbo.Bind();
		vbo.UpdateBuffer(mesh.GenerateData());

		ebo.Bind();
		ebo.UpdateBuffer(mesh.GenerateIndices());

		//Configuring VAO attributes
		vao.LinkAttribute(3, GL_FLOAT, 8, 0);   //vertex
		vao.LinkAttribute(3, GL_FLOAT, 8, 3);   //normal
		vao.LinkAttribute(2, GL_FLOAT, 8, 6);   //texture

		/* Texture binding */
		shader.use();
		for (int i = 0; i < textures.size(); ++i) {
			LogDebug("binding " + shader.ToString() + " with tex " + textures[i]->ToString() + " with id " + std::to_string(textures[i]->getTextureId()));
			textures[i]->BindToUnit(shader, (std::string("sampleTexture") + std::to_string(i)).c_str(), i);
		}
		glUseProgram(0);

		bool initialized{ vao.GetId() != 0 };
		if (!initialized) {
			LogError("failed to initialize draw call! id " + std::to_string(id));
		}
		else {
			LogDebug("generated draw call " + std::to_string(id));
		}
	}

	GladDrawCall::~GladDrawCall() {
	}

	void GladDrawCall::Draw() {
		LogDebug("update callback for drawcall " + std::to_string(id));
		update();

		LogDebug(
			string("Starting draw call [") + std::to_string(id)
			+ "] with " + shader.ToString()
			+ " || " + mesh.ToString()
			+ " || " + transform->ToString()
			+ " || " + camera.ToString()
			+ " || [" + std::to_string(textures.size()) + "] textures"
			+ " || " + vao.ToString()
			+ " || " + vbo.ToString()
			+ " || " + ebo.ToString()
			+ " with [" + std::to_string(ebo.size()) + "] indices"
		);

		shader.use();

		int i{ 0 };
		for (auto& texture : textures) {
			glActiveTexture(GL_TEXTURE0 + i++);
			texture->Bind();
		}
		glActiveTexture(GL_TEXTURE0);

		LogDebug("transforming camera");
		shader.setMat4("model", Math::GetModelMatrix(*transform));
		shader.setMat4("view", camera.GetView());
		shader.setMat4("projection", camera.GetProjection());
		
		vao.Bind();
		LogDebug("drawing");
		glDrawElements(GL_TRIANGLES, ebo.size(), GL_UNSIGNED_INT, NULL); // TODO switch  mode itd

		LogDebug("unbinding");
		vao.Unbind();
		glUseProgram(NULL);
		for (auto& texture : textures) {
			texture->Unbind();
		}
	}

	void GladDrawCall::Unload() {
	}
}