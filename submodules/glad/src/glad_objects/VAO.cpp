#include "GladObjects.h"

namespace mole::GLAD {

	VAO::VAO() :
		countAttributes{ 0 },
		GladBuffer{ "VAO" } {

		glGenVertexArrays(1, &id);
	}

	VAO::~VAO() {
		vector<GLuint> buffers{ id };
		glDeleteVertexArrays(buffers.size(), buffers.data());
	}

	void VAO::LinkAttribute(GLuint attributeSize, GLenum GL_DATA_TYPE, int stride, unsigned int offset) {

		GLuint attribute{ nextAttribute() };
		LogDebug("Enabling attribute " + std::to_string(attribute)
			+ ": size " + std::to_string(attributeSize)
			+ ", type " + std::to_string(GL_DATA_TYPE)
			+ ", stride " + std::to_string(stride)
			+ ", offset " + std::to_string(offset)
			+ " in " + ToString());

		CreatePointer(attribute, attributeSize, GL_DATA_TYPE, (GLboolean)GL_FALSE, (GLsizei)stride, offset);
		glEnableVertexAttribArray(attribute);
	}

	void VAO::Bind() {
		LogDebug("Binding " + ToString());
		glBindVertexArray(id);
	}

	void VAO::Unbind() {
		LogDebug("Unbinding " + ToString());
		glBindVertexArray(0);
	}

	unsigned int VAO::nextAttribute() {
		return countAttributes++;
	}

	void VAO::CreatePointer(GLuint attribute, GLint size, GLenum tGL, GLboolean normalize, GLsizei stride, unsigned int offset) {
		glVertexAttribPointer(
			attribute,							// vertex attribute (location == 0)
			size,								// size (sizeof(vec3) == 3)
			tGL,								// type (vec3 contains 3 GL_FLOAT)
			normalize,							// should the data be normalized (false by default)
			stride * sizeof(float),				// stride (vecX contains X floats, vec3position + vec3color + vec2texture)
			(void*)(offset * sizeof(float))		// offset (includes "weird cast")
		);
	}
}