#include "GladObjects.h"

namespace mole::GLAD {

	TBO::TBO() :
		GladBuffer("TBO") {

		glGenTextures(1, &id);
	}

	TBO::~TBO() {
		vector<GLuint> buffers{ id };
		glDeleteTextures(1, buffers.data());
	}

	void TBO::Generate(unsigned int size) {
		LogDebug("Allocating " + ToString());	
		glTexImage2D(GL_TEXTURE_2D, 0, GL_RGB, size, size, 0, GL_RGB, GL_UNSIGNED_BYTE, NULL);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
		glFramebufferTexture2D(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT0, GL_TEXTURE_2D, id, 0);
	}

	void TBO::Bind() {
		LogDebug("Binding " + ToString());
		glBindTexture(GL_TEXTURE_2D, id);
	}

	void TBO::Unbind() {
		LogDebug("Unbinding " + ToString());
		glBindRenderbuffer(GL_RENDERBUFFER, 0);
	}

	void TBO::UpdateBuffer(const vector<GLuint>& indices, unsigned int mode) {
		const GLsizeiptr size = indices.size() * sizeof(GLuint);
		const GLuint* data = indices.data();

		glBufferData(GL_ELEMENT_ARRAY_BUFFER, size, data, mode);
	}
}