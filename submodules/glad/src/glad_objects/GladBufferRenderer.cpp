#include "GladObjects.h"

namespace mole::GLAD {

	unsigned int GladBufferRenderer::nextId{ 0 };

	GladBufferRenderer::GladBufferRenderer(Generator* generator, unsigned int size) :
		vao{},
		vbo{},
		fbo{},
		tbo{},
		rbo{},
		shader{ *generator },
		id{ ++nextId } {

		vao.Bind();

		// No need to load geometry, it's a full screen square wrapped in a triangle
		vector<GLfloat> vertices{
			-2, -1, 0,   0,
			 0,  2, 0.5, 1,
			 2, -1, 1,   0,
		};
		vbo.Bind();
		vbo.UpdateBuffer(vertices);

		//Configuring VAO attributes
		vao.LinkAttribute(2, GL_FLOAT, 8, 0);   //vertex
		vao.LinkAttribute(2, GL_FLOAT, 8, 2);   //texture

		generator->SetTextureId(tbo.GetId());
		generator->use();
		LogDebug("binding " + shader.ToString() + " with tex " + generator->ToString() + " with id " + std::to_string(generator->getTextureId()));
		generator->BindToUnit(*generator, "tex", 0);
		glUseProgram(0);
		vao.Unbind();//must be BEFORE vbo and ebo
		vbo.Unbind();

		/* Buffer Section */
		fbo.Bind();
		tbo.Bind();
		tbo.Generate(size);
		rbo.Bind();
		rbo.Attach(size);


		bool initialized{ vao.GetId() != 0 };
		if (!initialized) {
			LogError("failed to initialize buffer renderer! id " + std::to_string(id));
		}
		else if (glCheckFramebufferStatus(GL_FRAMEBUFFER) != GL_FRAMEBUFFER_COMPLETE) {
			LogError("Framebuffer not properly created");
		}
		else {
			LogDebug("generated buffer render call " + std::to_string(id));
		}

		fbo.Unbind();
		rbo.Unbind();
	}

	GladBufferRenderer::~GladBufferRenderer() {
	}

	void GladBufferRenderer::Draw() {

		LogDebug(
			string("Starting texture buffering [") + std::to_string(id)
			+ "] " + shader.ToString()
			+ " || " + rbo.ToString());
		fbo.Bind();
		glEnable(GL_DEPTH_TEST);

		glClearColor(0, 0, 0, 1);
		glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
		Log::Restore();

		shader.use();
		tbo.Bind();
		vao.Bind();

		LogDebug("drawing");
		glDrawArrays(GL_TRIANGLES, 0, 6);

		glBindFramebuffer(GL_FRAMEBUFFER, 0);
		glDisable(GL_DEPTH_TEST);

		vao.Unbind();
		fbo.Unbind();
		glUseProgram(NULL);
	}

	void GladBufferRenderer::Unload() {
	}
}