#ifndef MOLEGLAD
#define MOLEGLAD

#include "MoleCommons.h"

namespace mole::GLAD {

	struct GladRenderer;

	class GladShader;
	class GladGenerator;

	class GladTexture;
	class GladCamera;
	class GladMesh;

	void SetActiveElements(Collection<Renderable> renderables, Collection<Generator> proceduralRendereres);
	void UnloadRenderables();

	struct ProtoVertex; // TO DO move to commons internal
}

struct mole::GLAD::GladRenderer {
	void Load(unsigned width, unsigned height);
	void Unload();
	void Render();
};

class mole::GLAD::GladShader : public mole::Shader {
	static unsigned int nextRef;
	unsigned int ref;
	unsigned int id;
	bool loaded;
public:
	GladShader(const char* vertexPath, const char* fragmentPath);
	~GladShader() override;
	void use() override;
	void Unload() override;
	void setBool(const string& name, bool value) const override;
	void setInt(const string& name, int value) const override;
	void setFloat(const string& name, float value) const override;
	void setVec3(const string& name, Vector3 value) const override;
	void setMat4(const string& name, float* value) const override;
	const unsigned int getShaderId() const override;
	const bool Loaded() const override;

	 const string ToString() const { return "\u001b[1;35mGlad Shader #" + std::to_string(ref) + "\u001b[1;0m"; };
};

class mole::GLAD::GladGenerator : public mole::Generator {
	static unsigned int nextRef;
	unsigned int ref;
	unsigned int textureType;
	unsigned int shaderId;
	unsigned int textureId;
	bool loaded;
public:
	GladGenerator(const char* vertexPath, const char* fragmentPath);
	~GladGenerator() override;
	// Shader
	void use() override;
	void setBool(const string& name, bool value) const override;
	void setInt(const string& name, int value) const override;
	void setFloat(const string& name, float value) const override;
	void setVec3(const string& name, Vector3 value) const override;
	void setMat4(const string& name, float* value) const override;
	const bool Loaded() const override;
	// Texture
	const unsigned int getShaderId() const override;
	const unsigned int getTextureId() const override;
	void Unload() override;
	void Bind() override;
	void Unbind() override;
	void BindToUnit(mole::Shader& shader, const char* uniform, unsigned int unit) override;
	// Generator
	void SetTextureId(unsigned int id) override;
	 const string ToString() const { return "\u001b[1;35mGlad Generator #" + std::to_string(ref) + "\u001b[1;0m"; };
};

class mole::GLAD::GladTexture : public mole::Texture {
	static unsigned int nextRef;
	unsigned int ref;
	unsigned int type;
	unsigned int id;
	bool loaded;
public:
	GladTexture(const RawTexture data, unsigned int width, unsigned int height, TextureInterpolation interpolation);
	~GladTexture() override;
	void Unload() override;
	void Bind() override;
	void Unbind() override;
	void BindToUnit(mole::Shader& shader, const char* uniform, unsigned int unit) override;
	const unsigned int getTextureId() const override;

	 const string ToString() const { return "\u001b[1;35mGlad Texture #" + std::to_string(ref) + "\u001b[1;0m"; };
};

class mole::GLAD::GladCamera : public mole::Camera {
	static unsigned int nextId;
	const unsigned int id;
	CameraMode m_mode;
	Transform m_transform;
	float m_fov;
	float* view;
	float* projection;

	const Vector3 front() const;
	const Vector3 up() const;
	const Vector3 right() const;

public:
	 const Transform& transform() const override;
	 const float fov() const override;
	 const CameraMode mode() const override;

	GladCamera(const char* path);
	~GladCamera() override;

	 void Translate(const Vector3 increment) override;
	 void Rotate(const Vector3 euler) override;
	 void SetFieldOfView(float fov) override;
	 void SetMode(CameraMode mode) override;
	 void SetTransform(const Vector3& position, const Vector3& rotation, const Vector3& scale) override;
	 float* GetView() override;
	 float* GetProjection() override;
	 const Vector3 GetDirection() const override;

	unsigned int getId() const;
	 const string ToString() const { return "\u001b[1;35mGlad Camera #" + std::to_string(id) + "\u001b[1;0m"; };
};

class mole::GLAD::GladMesh : public mole::Mesh {
	static unsigned int nextId;
	const unsigned int id;
	bool loaded;
	unsigned int smoothness;
	unsigned int vertexCount;
	vector<Vector3> vertices;
	vector<Vector3> normals;
	vector<Vector2> textures;
	vector<ProtoVertex> protoVertices;

	void Parse(const char* path, std::ifstream& stream);
public:
	GladMesh(const char* path);
	~GladMesh() override;

	void Unload() override;
	const vector<float> GenerateData() const override;
	const vector<unsigned int> GenerateIndices() const override;
	const unsigned int GetDataSize() const override;

	 const string ToString() const { return "\u001b[1;35mGlad Mesh #" + std::to_string(id) + "\u001b[1;0m"; };
};

struct mole::GLAD::ProtoVertex {
	const unsigned int vertex;
	const unsigned int normal;
	const unsigned int texture;

	ProtoVertex(
		const unsigned int vertex,
		const unsigned int normal,
		const unsigned int texture) :
		vertex{ vertex },
		normal{ normal },
		texture{ texture } {}
};

#endif //!MOLEGLAD