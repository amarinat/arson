#ifndef MOLE_GLAD_OBJECTS
#define MOLE_GLAD_OBJECTS

#include "moleGlad_internal.h"

namespace mole::GLAD {

	class GladBuffer;
	class VAO;
	class VBO;
	class EBO;
	class FBO;
	class RBO;
	class TBO;
	struct GladDrawCall;
	struct GladBufferRenderer;
}

class mole::GLAD::GladBuffer {

	static unsigned int nextRef;
	const unsigned int ref;
	const string name;
protected:
	GLuint id;
	GladBuffer(string name);
public:
	virtual ~GladBuffer();
	virtual void Bind() = 0;
	virtual void Unbind() = 0;
	const GLuint GetId() const;
	const string ToString() const;
};

class mole::GLAD::VAO : public GladBuffer {
	unsigned int countAttributes;
	unsigned int nextAttribute();
	void CreatePointer(GLuint attribute, GLint size, GLenum tGL, GLboolean normalize, GLsizei stride, unsigned int offset);
public:
	VAO();
	~VAO() override;
	void LinkAttribute(GLuint attributeSize, GLenum GL_DATA_TYPE, int stride, unsigned int offset);
	void Bind() override;
	void Unbind() override;
};

class mole::GLAD::VBO : public GladBuffer {
public:
	VBO();
	~VBO() override;
	void Bind() override;
	void Unbind() override;
	void UpdateBuffer(const vector<GLfloat>& vertices, int drawType = GL_STATIC_DRAW);
};

class mole::GLAD::EBO : public GladBuffer {
	unsigned int indexCount;
public:
	EBO();
	~EBO() override;
	void Bind() override;
	void Unbind() override;
	void UpdateBuffer(const vector<GLuint>& indices);
	const unsigned int size() const;
};

class mole::GLAD::FBO : GladBuffer {
	unsigned int width;
	unsigned int height;
public:
	FBO(unsigned int size = 64);
	~FBO() override;
	void Bind() override;
	void Unbind() override;
};

class mole::GLAD::RBO : public GladBuffer {
public:
	RBO();
	~RBO() override;
	void Attach(unsigned int size);
	void Bind() override;
	void Unbind() override;
	void UpdateBuffer(const vector<GLuint>& indices, unsigned int mode = GL_STATIC_DRAW);
};

class mole::GLAD::TBO : public GladBuffer {
public:
	TBO();
	~TBO() override;
	void Generate(unsigned int size);
	void Bind() override;
	void Unbind() override;
	void UpdateBuffer(const vector<GLuint>& indices, unsigned int mode = GL_STATIC_DRAW);
};

struct mole::GLAD::GladDrawCall {
	asset emmiter;
	GladDrawCall(asset emmiter, RenderableType type, Shader& shader, Mesh& mesh, Textures textures, Camera& camera, Transform* transform, Callback update);
	~GladDrawCall();
	void Draw();
	void Unload();
private:
	static unsigned int nextId;
	unsigned int id;
	VAO vao;
	VBO vbo;
	EBO ebo;
	Shader& shader;
	Mesh& mesh;
	Camera& camera;
	Textures textures;
	Transform* transform;
	Callback update;
};

struct mole::GLAD::GladBufferRenderer {
	GladBufferRenderer(Generator* generator, unsigned int size);
	~GladBufferRenderer();
	void Draw();
	void Unload();
private:
	static unsigned int nextId;
	unsigned int id;
	Shader& shader;
	VAO vao;
	VBO vbo;
	FBO fbo;
	RBO rbo;
	TBO tbo;
};

#endif // !MOLE_GLAD_OBJECTS