#ifndef MOLEGLAD_INTERNAL
#define MOLEGLAD_INTERNAL

#include "moleGlad.h"

extern "C" {
#define GLAD_IMPLEMENTATION
#include "glad/glad.h"
}

namespace mole::GLAD {



	void GenerateDrawCalls(Collection<Renderable> renderables);
	void GenerateBufferRenderers(Collection<Generator> renderers);

	void LogError(string message);
	void LogWarning(string message);
	void LogInfo(string message);
	void LogDebug(string message);

	void LogGladError(string message);
	void LogGladDebug(string message);
}

#endif //!MOLEGLAD_INTERNAL