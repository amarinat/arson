#ifndef MOLEMATH
#define MOLEMATH

#include "moleCommons.h"

namespace mole::Math {

	float* GetModelMatrix(const Transform& transform);
	float* GetViewMatrix(const Camera& camera);
	float* GeProjectionMatrix(CameraMode, float fov, float aspect, float near, float far);

	const Vector3 GetFront(const Camera& camera);
	const Vector3 GetUp(const Camera& camera);
	const Vector3 GetRight(const Camera& camera);

	const float mod(float value, const float period, const float lowerPeriod = 0.0f);
	const float clamp(float value, float max, float min);
}

#endif //!MOLEMATH