#ifndef MOLEMATH_INTERNAL
#define MOLEMATH_INTERNAL

#include "moleMath.h"

#include "glm.hpp"
#include "gtc/matrix_transform.hpp"
#include "gtc/type_ptr.hpp"

namespace mole::Math {

	using vec3 = glm::vec3;
	using mat4 = glm::mat4;

	constexpr mat4 identity{ 1.0f };
}

#endif //!MOLEMATH_INTERNAL