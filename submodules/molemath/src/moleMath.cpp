#include "moleMath_internal.h"


namespace mole {

	float* Math::GetModelMatrix(const Transform& transform) {

		static mat4 model;

		model = identity;

		model = scale(model, vec3(transform.scale.x, transform.scale.y, transform.scale.z));

		model = translate(model, vec3(transform.position.x, transform.position.y, transform.position.z));

		model = glm::rotate(model, glm::radians(transform.eulerRotation.x), vec3(1.0f, 0.0f, 0.0f));
		model = glm::rotate(model, glm::radians(transform.eulerRotation.y), vec3(0.0f, 1.0f, 0.0f));
		model = glm::rotate(model, glm::radians(transform.eulerRotation.z), vec3(0.0f, 0.0f, 1.0f));

		return value_ptr(model);
	}

	float* Math::GetViewMatrix(const Camera& c) {

		static mat4 view;

		const Vector3& p{ c.transform().position };
		const vec3 pos{ p.x, p.y, p.z };
		const float yaw{ c.transform().eulerRotation.x + 90 };
		const float pitch{ -c.transform().eulerRotation.y };

		glm::vec3 direction{};
		direction.x = cos(glm::radians(yaw)) * cos(glm::radians(pitch));
		direction.y = sin(glm::radians(pitch));
		direction.z = sin(glm::radians(yaw)) * cos(glm::radians(pitch));
		vec3 cameraFront = glm::normalize(direction);

		vec3 cameraDirection{ normalize(pos - direction) };
		vec3 up{ p.x, p.y + 1.0f, p.z };
		glm::vec3 cameraRight = glm::normalize(glm::cross(up, cameraDirection));

		glm::vec3 cameraUp = glm::cross(cameraDirection, cameraRight);
		view = lookAt(pos, pos + cameraFront, cameraUp);

		return value_ptr(view);
	}

	float* Math::GeProjectionMatrix(CameraMode mode, float fov, float aspect = 16 / 9, float near = 0.1f, float far = 100.0f) {

		static mat4 result;

		result = identity;

		switch (mode)
		{
		case CameraMode::Perspective:
			result = glm::perspective(glm::radians(fov), aspect, near, far);
			break;
		case CameraMode::Orthographic:
			result = glm::ortho(glm::radians(fov), aspect, near, far);
			break;
		default:
			throw std::exception("option not implemented"); // TO DO warning + return matrix identity
			break;
		}

		return value_ptr(result);
	}

	const Vector3 Math::GetFront(const Camera& c) {

		const float yaw{ glm::radians(c.transform().eulerRotation.x + 90) };
		const float pitch{ glm::radians(c.transform().eulerRotation.y) };

		const float sinYaw{ sin(yaw) };
		const float cosYaw{ cos(yaw) };
		const float sinPitch{ sin(pitch) };
		const float cosPitch{ cos(pitch) };

		const vec3 direction{ cosYaw * cosPitch, sinPitch, sinYaw * cosPitch };
		const vec3 normalized{ normalize(direction) };

		return { normalized.x, normalized.y, normalized.z };
		return { 0,0,0 };
	}

	const Vector3 Math::GetUp(const Camera& c) {

		const float yaw{ glm::radians(c.transform().eulerRotation.x) };
		const float pitch{ glm::radians(-c.transform().eulerRotation.y) };

		const float sinYaw{ sin(yaw) };
		const float cosYaw{ cos(yaw) };
		const float sinPitch{ sin(pitch) };
		const float cosPitch{ cos(pitch) };

		const vec3 direction{ cosYaw * cosPitch, cosPitch, sinYaw * cosPitch };
		const vec3 normalized{ normalize(direction) };

		return { normalized.x, normalized.y, normalized.z };
	}

	const Vector3 Math::GetRight(const Camera& c) {

		const float yaw{ glm::radians(c.transform().eulerRotation.x + 90.0f) };

		const float sinYaw{ sin(yaw) };
		const float cosYaw{ cos(yaw) };

		const vec3 direction{ sinYaw, 0, cosYaw };
		const vec3 normalized{ normalize(direction) };

		return { normalized.x, normalized.y, normalized.z };
	}

	const float Math::mod(float value, const float period, const float lowerThreshold) {

		std::function<float()> increment = [=] {return value + period; };
		std::function<float()> decrement = [=] {return value - period; };
		std::function<bool()> bigger = [&] { return value > period; };
		std::function<bool()> smaller = [&] { return value < lowerThreshold; };

		while (bigger() || smaller()) {
			value = bigger() * decrement() + smaller() * increment();
		}

		return value;
	}

	const float Math::clamp(float value, float max, float min) {
		std::function<bool()> bigger = [=] { return value > max; };
		std::function<bool()> smaller = [=] { return value < min; };

		return value * !bigger() * !smaller() + max * bigger() + min * smaller();
	}
}
