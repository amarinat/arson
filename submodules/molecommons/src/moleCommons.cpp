#include "moleCommons_internal.h"

#include <iostream>

namespace mole {

#ifdef _MOLERELEASE
	Verbosity Log::level{ Verbosity::Silent };
#else
#ifdef _MOLEWARNING
	Verbosity Log::level{ Verbosity::Warnings };
#else
#ifdef _MOLEINFO
	Verbosity Log::level{ Verbosity::Info };
#else
#ifdef _MOLEDEBUG
	Verbosity Log::level{ Verbosity::Debug };
#else
	Verbosity Log::level{ Verbosity::ErrorOnly };
#endif // _MOLEDEBUG
#endif // _MOLEINFO
#endif // _MOLEWARNING
#endif // _MOLERELEASE
	Verbosity Log::regularLevel{ Log::level };

	unsigned int mole::Transform::nextId{0};

	void mole::ConsoleOutput(const char* message) {
		std::cout << message;
	}

	void mole::FileOutput(const char* message) {
		// TO DO
	}


	void Log::Output(string type, string caller, string message) {
		ConsoleOutput(string("[" + type + " " + caller + "] \"" + message + "\"\n").c_str());
	}

	void Log::SetVerbosity(Verbosity verbosityLevel) {
		level = verbosityLevel;
	}

	Verbosity Log::GetVerbosity() {
		return level;
	}

	void Log::EnforceDebug() {
		regularLevel = level;
		level = Verbosity::Debug;
	}

	void Log::Restore() {
		level = regularLevel;
	}

	void Log::Error(string caller, string message) {
		if (level >= Verbosity::ErrorOnly) {
			Output("\u001b[1;31mERROR\u001b[0m", caller, message);
		}
	}

	void Log::Warning(string caller, string message) {
		if (level >= Verbosity::Warnings) {
			Output("\u001b[1;33mWARNING\u001b[0m", caller, message);
		}
	}

	void Log::Info(string caller, string message) {
		if (level >= Verbosity::Info) {
			Output("\u001b[1;32mINFO\u001b[0m", caller, message);
		}
	}

	void Log::Debug(string caller, string message) {
		if (level >= Verbosity::Debug) {
			Output("\u001b[1;34mDEBUG\u001b[0m", caller, message);
		}
	}

	void Log::Bypass(string message) {
		ConsoleOutput(message.c_str());
	}
}
