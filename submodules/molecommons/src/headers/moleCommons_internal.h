#ifndef MOLECOMMONS_INTERNAL
#define MOLECOMMONS_INTERNAL

#include "moleCommons.h"

namespace mole {

	void ConsoleOutput(const char* message);
	void FileOutput(const char* message);
}

#endif //!MOLECOMMONS_INTERNAL