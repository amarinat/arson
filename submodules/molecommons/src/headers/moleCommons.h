#ifndef MOLECOMMONS
#define MOLECOMMONS

#include <string>
#include <memory>
#include <functional>
#include <map>

namespace mole {

	using Process = std::function<void(const char*)>;
	using Callback = std::function<void(void)>;
	using RawTexture = unsigned char*;
	using string = std::string;
	using asset = std::string;
	template<class T> using vector = std::vector<T>;
	template<class T> using owned = std::unique_ptr<T>;
	template<class K, class T> using map = std::map<K, T>;
	template<class T> using Repository = vector<owned<T>>;
	template<class K, class T> using Locator = std::map<K, T*>;
	template<class T> using Collection = vector<T*>;

	enum class Verbosity;
	enum class CameraMode;
	enum class RenderableType;
	enum class TextureInterpolation;
	enum class Resource;

	/*
	template<class R> class component {
		static inline unsigned int nextId{};
	public:
		const unsigned int id;
		component() :
			id{ ++nextId } {}
	};
	*/

	struct Log;
	struct Vector2;
	struct Vector3;
	struct Transform;
	struct Shader;
	struct Texture;
	struct Generator;
	struct Mesh;
	struct Camera;
	struct Renderable;

	using Textures = Collection<Texture>;
}

enum class mole::Verbosity { Silent = 0, ErrorOnly = 1, Warnings = 2, Info = 3, Debug = 4 };
enum class mole::CameraMode { Perspective = 0, Orthographic = 1 };
enum class mole::TextureInterpolation { Closest = 0, Linear = 1 };

enum class mole::RenderableType {
	Sprite,
	Model,
	Wireframe,
	Texture,
};

enum class mole::Resource {
	Shader,
	Procedural,
	Texture,
	Mesh,
	Camera,
	Transform,
};

struct mole::Vector2 {
	float x;
	float y;
};

struct mole::Vector3 {
	float x;
	float y;
	float z;
};

struct mole::Log {

	static void SetVerbosity(Verbosity level);
	static Verbosity GetVerbosity();
	static void EnforceDebug();
	static void Restore();

	static void Error(string caller, string message);
	static void Warning(string caller, string message);
	static void Debug(string caller, string message);
	static void Info(string caller, string message);
	static void Bypass(string message);
private:
	static void Output(string type, string caller, string message);
	static Verbosity level;
	static Verbosity regularLevel;
};

struct mole::Transform {
	Vector3 position;
	Vector3 scale;
	Vector3 eulerRotation;
	const unsigned int id;

	Transform() :
		id{ ++nextId },
		position{ 0,0,0 },
		scale{ 1,1,1 },
		eulerRotation{ 0,0,0 } {}
	~Transform() {}

	const string ToString() const { return "\u001b[1;35mtransform #" + std::to_string(id) + "\u001b[1;0m"; };
private:
	static unsigned int nextId;
};

struct mole::Shader {

	virtual ~Shader() {}

	virtual void use() = 0;
	virtual void Unload() = 0;
	virtual void setBool(const string& name, bool value) const = 0;
	virtual void setInt(const string& name, int value) const = 0;
	virtual void setFloat(const string& name, float value) const = 0;
	virtual void setVec3(const string& name, Vector3 value) const = 0;
	virtual void setMat4(const string& name, float* value) const = 0;
	virtual const unsigned int getShaderId() const = 0;
	virtual const bool Loaded() const = 0;

	virtual const string ToString() const = 0;
};

struct mole::Mesh {
	virtual ~Mesh() {}
	virtual void Unload() = 0;
	virtual const vector<float> GenerateData() const = 0;
	virtual const vector<unsigned int> GenerateIndices() const = 0;
	virtual const unsigned int GetDataSize() const = 0;

	virtual const string ToString() const = 0;
};

struct mole::Texture {
	virtual ~Texture() {}
	virtual const unsigned int getTextureId() const = 0;
	virtual void Unload() = 0;
	virtual void Bind() = 0;
	virtual void Unbind() = 0;
	virtual void BindToUnit(Shader& shader, const char* uniform, unsigned int unit) = 0;

	virtual const string ToString() const = 0;
};

struct mole::Renderable {
	Transform& transform;

	const RenderableType type;
	const asset name;
	Shader* shader;
	Mesh* mesh;
	Camera* camera;
	Textures textures;
	Callback update;

	Renderable(asset name, RenderableType type, Shader* shader, Mesh* mesh, Textures textures, Camera* camera, Transform& transform, Callback update) :
		type{ type },
		name{ name },
		mesh{ mesh },
		textures{ textures },
		camera{ camera },
		shader{ shader },
		update{ update },
		transform{ transform } {}
	~Renderable() {}
};

struct mole::Camera {

	virtual const Transform& transform() const = 0;
	virtual const float fov() const = 0;
	virtual const CameraMode mode() const = 0;

	virtual ~Camera() = default;

	virtual void Translate(const Vector3 increment) = 0;
	virtual void Rotate(const Vector3 increment) = 0;
	virtual void SetFieldOfView(float fov) = 0;
	virtual void SetMode(CameraMode mode) = 0;
	virtual void SetTransform(
		const Vector3& position,
		const Vector3& rotation,
		const Vector3& scale) = 0;
	virtual float* GetView() = 0;
	virtual float* GetProjection() = 0;
	virtual const Vector3 GetDirection() const = 0;

	virtual const string ToString() const = 0;
};

struct mole::Generator :
	public Shader,
	public Texture {

	virtual void SetTextureId(unsigned int id) = 0;
	virtual const string ToString() const = 0;
};

#endif //!MOLECOMMONS